#!/usr/bin/env python

import sys
import ROOT

fin = ROOT.TFile(sys.argv[1], 'read')
fout = ROOT.TFile(sys.argv[2], 'recreate')
if len(fin.GetListOfKeys())  == 0:
  fin.Close()
  fout.Close()
  sys.exit(0)
t = [fin.Get(x.GetName()) for x in fin.GetListOfKeys()][0]

fout.cd()
newt = t.CloneTree(0)
for event in xrange(t.GetEntries()):
  t.GetEntry(event)
  if t.Reconstructed_1__nSelectedJet == 2 and  t.Reconstructed_1__nSelectedBJet == 1:
    newt.Fill()
newt.Write()

fin.Close()

fout.Close()

