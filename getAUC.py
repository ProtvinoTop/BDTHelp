#!/usr/bin/env python

import sys
import ROOT

f = ROOT.TFile(sys.argv[1])

name = [x.GetName() for x in f.Get("Method_BDT").GetListOfKeys()][0]
histname = [x.GetName() for x in f.Get("Method_BDT/" + name).GetListOfKeys() if x.GetName()=="MVA_" + name + "_rejBvsS"][0]

print name, "{0:3.2f}".format(f.Get("Method_BDT/" + name + "/" + histname).Integral() - 50)

