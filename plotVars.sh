#!/bin/bash

scale=614.5
rootdir=root0109
PERC=0.01
[ -z "$1" ] && dr="." || dr="$1"

cd $dr
test -d varplots && tar --remove-files -czf varplots.tar.gz varplots
mkdir varplots
rsync -avz $HOME/BDTIssues/TMVAReader/ varplots/
cd varplots

xmlfile="$(ls ../weights/*.xml)"
varfile="../variables.txt"


source setup.sh
DISPLAY="" ionice -c 3 ./TMVAReader.py \
 --rootdir $rootdir \
 --weightxml $xmlfile \
 --variables $varfile \
 --mcscale $scale \
 --perc $PERC \
 --databranch SingleMuon_Run2015D-05Oct2015-v1 \
DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8 \
QCD_Pt-20toInf_MuEnrichedPt15_TuneCUETP8M1_13TeV_pythia8 \
ST_t-channel_4f_leptonDecays_13TeV-amcatnlo-pythia8_TuneCUETP8M1 \
ST_t-channel_antitop_4f_leptonDecays_13TeV-powheg-pythia8_TuneCUETP8M1 \
ST_t-channel_top_4f_leptonDecays_13TeV-powheg-pythia8_TuneCUETP8M1 \
ST_tW_antitop_5f_inclusiveDecays_13TeV-powheg-pythia8_TuneCUETP8M1 \
ST_tW_top_5f_inclusiveDecays_13TeV-powheg-pythia8_TuneCUETP8M1 \
TTJets_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8 \
TT_TuneCUETP8M1_13TeV-powheg-pythia8_ext \
TT_TuneZ2star_13TeV-powheg-pythia6-tauola \
WJetsToLNu_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8 \
WJetsToLNu_TuneCUETP8M1_13TeV-madgraphMLM-pythia8 \

bash $HOME/BDTIssues/RatioPlot/plotall.sh

