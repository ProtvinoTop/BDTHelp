#!/bin/bash
# This file prints command to dump Matthias pxlio files
# as well as one other crab results via lcg-cp

BASEPATH="/storage/data/cms/store/user/mkomm/DX_13"
SRMHOST="srm://ingrid-se02.cism.ucl.ac.be:8444/srm/managerv2?SFN="
LCGCP_OPT="-n 5"

VOMS_CMD="voms-proxy-init --voms cms"


for dirn in `lcg-ls "${SRMHOST}${BASEPATH}"`; do
  subname="$(echo $dirn | sed "s,$BASEPATH/,,g")"
  mkdirname=$subname
  while ! lcg-ls "${SRMHOST}${BASEPATH}/$subname" | grep -q pxlio ; do
    subname="$(lcg-ls ${SRMHOST}${BASEPATH}/$subname | sed "s,$BASEPATH/,,g")"
  done
  echo $VOMS_CMD | tee $mkdirname.sh
  echo mkdir -p "$mkdirname" | tee -a $mkdirname.sh
  for f in $(lcg-ls "${SRMHOST}${BASEPATH}/$subname"); do
    echo lcg-cp $LCGCP_OPT "${SRMHOST}$BASEPATH/$subname/$(basename $f)" "$mkdirname/$(basename $f)" | tee -a $mkdirname.sh
  done
done
