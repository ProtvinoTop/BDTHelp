IHEP Torque
=========

Dumps files: dump.sh
-----------

<pre>
Commands are just printed to stdout and separate files *.sh
To execute them use screen (!) and parallel utility:

  export PATH=/afs/ihep.su/user/l/latyshgs/public/sw/parallel/bin:$PATH
  cat <sh file> | parallel -j 8 -k
</pre>

Runs pxlrun file-by-file
------------

Prepare xml.template
  
  * export PXLTORQUE to path up to Diff13 
  * copy torque folder to $PXLTORQUE
  * replace file path to %FILENAME%
  * check torque/ files
  * run the same

<pre>
for p in WJetsToLNu_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8 \
         TT_TuneCUETP8M1_13TeV-powheg-pythia8_ext \
         ST_t-channel_4f_leptonDecays_13TeV-amcatnlo-pythia8_TuneCUETP8M1 \
         ST_tW_antitop_5f_inclusiveDecays_13TeV-powheg-pythia8_TuneCUETP8M1 \
         DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8 \
         TT_TuneZ2star_13TeV-powheg-pythia6-tauola
do
  find /lustre/ihep.su/tmp/latyshgs/$p -type f -name '*.pxlio' | while read i; do 
    FILE=$i qsub pxlrun.sh
  done
done 
</pre>

  * After jobs finished (qstat -a | grep `id -un`), check root files in $ROOTOUT (from torque/settings.sh)
  * use hadd to union *test* and *training* files to output-test.root and output-training.root

Prepare CMSSW for IHEP
-------------

The same as for lxplus but do prior
  
  I use rsync to copy ~/.pxl-3.5 to IHEP UI instead of rebuilding

  Don't forget:
  <pre>export PATH=/cvmfs/cms.cern.ch/bin:$PATH</pre>