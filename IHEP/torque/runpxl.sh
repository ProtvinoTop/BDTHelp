#!/bin/bash

export PATH=/cvmfs/cms.cern.ch/bin:$PATH
export SCRAM_ARCH=slc6_amd64_gcc491
export CMSSWVERSION="CMSSW_7_4_15"
cd $PXLTORQUE/Diff13/CMSSW/CMSSW_7_4_15
echo "cmsenv ..."
eval `scramv1 runtime -sh`
cd $PXLTORQUE/Diff13
echo "setVars ..."
pwd
source pxl-env.sh
source $PXLTORQUE/torque/settings.sh

echo "pxlrun ..."
tmp=$(mktemp --tmpdir=/tmp)
DAT=$(uuidgen -r)
FILE=$1
updir="$ROOTOUT/$(basename $(dirname $FILE))"
mkdir -p "$updir"
cd "$updir"
cat $PXLTORQUE/torque/analysisflow.xml.template | sed -e "s,%FILENAME%,$FILE,g" -e "s,%IDX%,$DAT,g" | cat > $tmp
echo "grep test"
echo $tmp
grep lustre $tmp

echo "$FILE => $DAT" >> filemap.txt

ionice -c 3 pxlrun $tmp 2>> $updir/failed_jobs.files.log || echo $FILE >> $updir/failed_jobs.files.txt
