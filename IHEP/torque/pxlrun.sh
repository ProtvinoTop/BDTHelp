#!/bin/sh
#
# Output files for stdout and stderr.
#
#PBS -o /afs/ihep.su/user/l/latyshgs/matt/pxlrun/batch/stdout.log
#PBS -e /afs/ihep.su/user/l/latyshgs/matt/pxlrun/batch/stderr.log
#
# Job's name is example
#PBS -N pxlrun
# Submit the job to this queue.
#PBS -m a
#PBS -M thegriglat@ihep.ru
#PBS -q ihep-short
#PBS -V

bash $PXLTORQUE/torque/runpxl.sh $FILE
