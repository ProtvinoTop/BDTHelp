#!/bin/bash

/afs/cern.ch/user/g/glatyshe/work/public/top/matthias/getplots.py "$1"

cd $(dirname $1)
ROOTSYS=$HOME root -l -b -q ~/tmva/test/plotall.C
ROOTSYS=$HOME root -l -b -q ~/tmva/test/correlations.C
#ROOTSYS=$HOME root -l -b -q ~/tmva/test/mvaeffs.C
ROOTSYS=$HOME root -l -b -q ~/tmva/test/efficiencies.C
ROOTSYS=$HOME root -l -b -q ~/tmva/test/BDTControlPlots.C
ROOTSYS=$HOME root -l -b -q ~/tmva/test/mvas.C\(\"TMVA.root\",3\)
