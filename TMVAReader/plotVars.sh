#!/bin/bash

databarn=553.1
testpart=0.9
rootdir=root0109
PERC=1
[ -z "$1" ] && dr="." || dr="$1"

cd $dr
test -d varplots && tar --remove-files -czf varplots.$(date +"%F-%s").tar.gz varplots
mkdir varplots
rsync -avzc --delete $HOME/BDTIssues/TMVAReader/ varplots/
cd varplots

xmlfile="$(ls ../weights/*.xml)"
varfile="../variables.txt"


source setup.sh
DISPLAY="" ionice -c 3 ./TMVAReader.py \
 --rootdir $rootdir \
 --weightxml $xmlfile \
 --variables $varfile \
 --databarn $databarn \
 --testpart $testpart \
 --perc $PERC \
 --mvacut 0.0 \
 --databranch SingleMuon_Run2015D-05Oct2015-v1 \
DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8 \
QCD_Pt-20toInf_MuEnrichedPt15_TuneCUETP8M1_13TeV_pythia8:0.18 \
ST_t-channel_antitop_4f_leptonDecays_13TeV-powheg-pythia8_TuneCUETP8M1 \
ST_t-channel_top_4f_leptonDecays_13TeV-powheg-pythia8_TuneCUETP8M1 \
ST_tW_antitop_5f_inclusiveDecays_13TeV-powheg-pythia8_TuneCUETP8M1 \
ST_tW_top_5f_inclusiveDecays_13TeV-powheg-pythia8_TuneCUETP8M1 \
TT_TuneCUETP8M1_13TeV-powheg-pythia8_ext \
WJetsToLNu_TuneCUETP8M1_13TeV-madgraphMLM-pythia8

for i in *.root ; do
  echo $i | grep -q rej-power && continue
  root -l -b -q $HOME/BDTIssues/RatioPlot/myMacro_legendInside.C\(\"$i\"\)
done
