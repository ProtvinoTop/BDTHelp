#!/usr/bin/env python

import sys
import os
import argparse
import array
import math
import glob
import copy

import ROOT

### CONSTANTS
THMIN = -1
THMAX = +1
NDIVISIONS = 40
### END

def main():
  parser = argparse.ArgumentParser(description='TMVAReader plotter')
  parser.add_argument('--variables', dest='variablefile', action='store', default="variables.txt", help="file variables.txt")
  parser.add_argument('--databranch', dest="databranch", action='store', default="SingleMuon_Run2015D-05Oct2015-v1", help="Name of data branch")
  parser.add_argument('--rootdir', dest="rootdir", action='store', default=".", help="Directory with ROOT files")
  parser.add_argument('--weightxml', dest="weightxml", action='store', default=".", help="Weight xml file")
  parser.add_argument('--databarn', dest="databarn", action='store', default=553.1, type=float, help="Data pb")
  parser.add_argument('--testpart', dest="testpart", action='store', default=1.0, type=float, help="Part testing data")
  parser.add_argument('--perc', dest="eventsperc", action='store', default=1.0, type=float, help="Use only given percent of events")
  parser.add_argument('--mvacut', dest="mvacut", action='store', default=0.0, type=float, help="Cut on MVA output")
  parser.add_argument('--TMVAlib', dest="TMVAlib", action='store', default="/afs/cern.ch/user/g/glatyshe/work/public/top/matthias/Diff13/PxlModules/build/internal/TMVA/libTMVA.so", help="Path to TMVA lib")
  parser.add_argument('--progress','-p', dest='progress', action='store_true', default=False)
  parser.add_argument('mcBranch', metavar='mcBranch',nargs='+', help='MC branches')
  
  args = parser.parse_args()
  ROOT.TMVA.Tools.Instance()
  
  branchw = {}
  
  def getColor(name):
    if "WJets" in name:
      return ROOT.kGreen - 2
    elif "t-channel" in name:
      return ROOT.kRed
    elif "tW" in name:
      return ROOT.kOrange
    elif "QCD" in name:
      return ROOT.kGray
    elif "TT" in name:
      return ROOT.kOrange - 3
    return ROOT.kBlue
  
  def getLegendName(n):
    if "WJets" in n:
      return "W+Jets"
    elif "TT" in n:
      return 't#bar{t}'
    elif "SingleMuon" in n:
      return "Data"
    elif "t-channel" in n:
      return "t-channel"
    elif "QCD" in n:
      return "QCD"
    elif "DYJets" in n:
      return "Drell-Yan"
    elif "ST_tW" in n:
      return "t+W"
    return n   

  def getBinInterval(n):
    if "dR" in n:
      return (0, 8)
    elif "Phi" in n or "Eta" in n:
      maxx = 5
      if "abs" in n:
        minx = 0
      else:
        minx = -maxx
      return (minx, maxx)
    elif "isotropy" in n:
      return (0, 1)
    elif 'sphericity' in n:
      return (0, 1)
    elif "n60" in n:
      return (0, 40)
    elif "n90" in n:
      return (0, 60)
    elif "planarit" in n:
      return (-15, 0)
    elif "cos" in n.lower():
      return (-1, 1)
    elif "Top" in n:
      if "Mass" in n:
        return (0, 500) 
      elif "Sqrt" in n:
        return (0, 900)
      return (0, 300)
    else:
      return (0, 200)


  TBranches = {} 
  variables = {}
  files = []
  allBranches = [] 
  for br in args.mcBranch + [args.databranch]:
    branchname = br.split(":")[0]
    if len(br.split(":")) > 1:
      branchwe = float(br.split(":")[1])
    else:
      branchwe = 1
    allBranches.append(branchname)
    branchw[branchname] = branchwe
    
  for i in glob.glob(os.path.join(args.rootdir, '*.root')):
    f = ROOT.TFile(i)
    branch = [k.GetName() for k in f.GetListOfKeys()][0]
    if branch in allBranches:
      if branch not in TBranches.keys():
        TBranches[branch] = []
      TBranches[branch] += [f.Get(branch)]
      files.append(f)
      print "File", i, "has been added."
  
  for branch in allBranches:
    if not branch in TBranches:
      print "Cannot import branch " + str(branch)
      exit(1)
    else:
      print "Branch", branch, "will be used"
       
  reader = ROOT.TMVA.Reader("!V:!Color")
  for var in [x.strip() for x in open(args.variablefile, 'r').readlines()]:
    if var == "" or var.startswith("#"):
      continue
    variables[var] = array.array('f', [0])
    reader.AddVariable(var, variables[var])
    
  for var in [x.strip() for x in open(os.path.join(os.path.dirname(args.variablefile), 'spectators.txt'), 'r').readlines()]:
    if var == "" or var.startswith("#"):
      continue
    variables[var] = array.array('f', [0])
    reader.AddSpectator(var, variables[var])
  
  print "Booking MVA method ..."
  methodname = open(args.weightxml, 'r').readlines()[1].strip().split('"')[1].split("::")[1]
  print "  Method name: " + methodname
  
  reader.BookMVA(methodname, args.weightxml)
  databranch = [x for x in TBranches.keys() if "SingleMuon" in x][0]
  # data goes first
  OrderedBranches = [databranch] + list(set(TBranches.keys()) - set([databranch]))
  print "OrderedBranches:", OrderedBranches
  
  for k in TBranches.keys():
    print k, "|events:",  sum([x.GetEntries() for x in TBranches[k]]), "|weight: ",branchw[k]

  ###
  #  Histograms
  #   MVA_output
  #    data
  #    MC
  #      mc1
  #      mc2 
  #      mc3
  #   var1
  #   var2
  ###
  DDhistos = {'data' : {}, 'mc' : {}}
  Histograms = {}
  ROCs = {}
  for i in ["MVA_output"] + variables.keys():
    Histograms.update({i : {"data" : {}, "mc" : {}}})
  Histograms["MVA_output"]['mc']['tt_WJets'] = ROOT.TH1D("MVA_output_tt_WJets", "BDT tt_WJets" , 100, THMIN, THMAX)
  ROCs['tt_WJets'] = ROOT.TH1D("MVA_output_tt_WJets", "BDT_tt_WJets", 100, -1, 1)
  for br in OrderedBranches:
    print "Branch: " + br
    datatype = "data" if "SingleMuon" in br else "mc"
    if datatype == 'mc':
      ROCs[br] = ROOT.TH1D("MVA_output_" + br, "BDT_" + br, 100, -1, 1)
    color = getColor(br)
    hout = ROOT.TH1D("MVA_output_{0}".format(br), "BDT " + getLegendName(br), 100, THMIN, THMAX)
    Histograms["MVA_output"][datatype].update({br: hout})
    hout.SetFillColor(color)
    hout.SetLineColor(color)
    for v in variables.keys():
      minx, maxx = getBinInterval(v)
      h = ROOT.TH1D(br + v, getLegendName(br), NDIVISIONS, minx, maxx)
      Histograms[v][datatype].update({br: h})
      h.SetFillColor(color)
      h.SetLineColor(color)
      if v not in DDhistos[datatype].keys():
        DDhistos[datatype][v] = ROOT.TH2D(v, v, 40, -1, 1, 50, minx, maxx)
    for fbr in TBranches[br]:
      entries = int(fbr.GetEntries() * args.eventsperc)
      try:
        bw = branchw[fbr.GetName()]
      except:
        bw = 1
      print "Initial branch weight:", bw
  
      for entry in xrange(entries):
        nb = fbr.GetEntry(entry)
        if nb <= 0:
          print "Cannot get entry"
          continue
        ## mtw cut
#        if fbr.SingleTop_1__mtw_beforePz < 50:
#          continue
        for v in variables.keys():
          variables[v][0] = getattr(fbr, v)
        mvaOutput = reader.EvaluateMVA(methodname)
        weight = bw
  
        try:
            if "SingleMuon" not in br:
              weight *= args.databarn / args.testpart * math.copysign(fbr.mc_weight, fbr.Generated_1__genweight) 
            else:
              weight *= 1
        except:
          weight *= 1
        hout.Fill(mvaOutput, weight)
        if datatype == "mc":
          ROCs[br].Fill(mvaOutput)
          allbr = [x for x in TBranches.keys() if "TT_Tune" in x or "WJets" in x]
#          if "TT_Tune" in br or "WJets" in br:
            #ROCs['tt_WJets'].Fill(mvaOutput, TBranches[br].GetEntries() / (TBranches[br].GetEntries() + TBranches[(set(allbr) - set([br]))[0]].GetEntries()))
#        if mvaOutput < args.mvacut:
          # reject background
#          continue
        for v in variables.keys():
          Histograms[v][datatype][br].Fill(variables[v][0], weight)
          DDhistos[datatype][v].Fill(mvaOutput, variables[v][0], weight)
        if args.progress and entry % 100 == 0:
          sys.stdout.write("{0:3.3f} %\r".format(100 * (entry + 1) / float(entries )))
          sys.stdout.flush()
      print ""

  # try to prepare single-rejection-power ROC plots
  srpf = ROOT.TFile('single-rej-power.root', 'recreate')
  srpf.cd()
  c = ROOT.TCanvas()
  for br in [x for x in TBranches.keys() if 'SingleMuon' not in x and 't-channel' not in x] + ["tt_WJets"]:
    tchkey = [x for x in TBranches.keys() if "t-channel" in x][0]
    hsignal = Histograms['MVA_output']['mc'][tchkey].Clone()
    #hbckg   = Histograms['MVA_output']['mc'][br].Clone()
    hbckg   = ROCs[br].Clone()
    if hsignal.GetEntries() == 0 or hbckg.GetEntries == 0:
      continue
    print 'try to book roc class for', br
    roc = ROOT.TMVA.ROCCalc(hsignal, hbckg)
    print "ROC Integral:", roc.GetROCIntegral()
    t = ROOT.TText(0.1, 0.2, "AUC: {0:0.2f}%".format(roc.GetROCIntegral() * 100 - 50  ))
    print "try to get roc"
    hroc = roc.GetROC()
    hroc.SetTitle(br)
    hroc.SetMinimum(0)
    hroc.SetMaximum(1)
    hroc.Clone("ROC_" + br).Write()
    hroc.Draw()
    t.Draw()
    c.SaveAs("ROC_" + br + ".png")
  srpf.Close()
  #del c
    
  for v in variables.keys() + ["MVA_output"]:
    Histograms[v]['data'][Histograms[v]["data"].keys()[0]].GetXaxis().SetTitle(v)
    
  for key in Histograms.keys():
    outf = ROOT.TFile(key+".root", "RECREATE")
    outf.cd()
    
    print "plotting " + key
    c = ROOT.TCanvas()
    c.cd()
    hdata = Histograms[key]["data"][Histograms[key]["data"].keys()[0]]  # only one data is allowed
    thstack = ROOT.THStack(key, key)
    ths = sorted(Histograms[key]["mc"].values(), key=lambda x: x.GetName())
    QCDidx = [ths.index(x) for x in ths if "QCD" in x.GetName()][0]
    ths = ths + [ths.pop(QCDidx)]
    for th in ths:
      thstack.Add(th)
  
    htmp1 = hdata.Clone("data")
    htmp1.Write()
    htmp2 = thstack.Clone("MCStack")
    htmp2.Write()
    for i in ('data', 'mc'):
      if key == "MVA_output":
        continue
      try:
        dx = DDhistos[i][key].ProfileX().Clone(i + "_" + key + "_x")
        dy = DDhistos[i][key].ProfileY().Clone(i + "_" + key + "_y")
        dx.Write()
        dy.Write()
      except:
        print "  cannot profile ..."
      DDhistos[i][key].Clone("2D_" + key).Write()
    if key != "MVA_output":
      #profileX
      h1 = DDhistos['mc'][key].ProfileX().Clone()
      h2 = DDhistos['data'][key].ProfileX().Clone()
      h1.SetLineColor(ROOT.kAzure + 1)
      h1.SetFillColor(ROOT.kAzure + 1)
      h1.SetOption("E2")
      h2.SetLineColor(ROOT.kBlack)
      ts1 = ROOT.THStack('mc_vs_data_x', 'mc_vs_data_x')
      ts1.Add(h2)
      ts1.Add(h1)
      ts1.Clone().Write()
      #profileY
      h1 = DDhistos['mc'][key].ProfileY().Clone()
      h2 = DDhistos['data'][key].ProfileY().Clone()
      h1.SetLineColor(ROOT.kAzure + 1)
      h1.SetFillColor(ROOT.kAzure + 1)
      h1.SetOption("E2")
      h2.SetLineColor(ROOT.kBlack)
      ts1 = ROOT.THStack('mc_vs_data_y', 'mc_vs_data_y')
      ts1.Add(h2)
      ts1.Add(h1)
      ts1.Clone().Write()
    outf.Close()
  
#    thstack.Draw("hist")
#    hdata.Draw("e same")
 
if __name__ == "__main__":
  main() 
