#!/usr/bin/env python
#
# getplots.py file file file ....
import sys
import os
import ROOT


def plotStacked(filename, signal, bckg):
  c = ROOT.TCanvas()
  c.cd()
  signal.SetFillColor(ROOT.kBlue)
  signal.SetLineColor(ROOT.kBlue)
  signal.SetFillStyle(3354)
  bckg.SetFillColor(ROOT.kRed)
  bckg.SetLineColor(ROOT.kRed)
  bckg.SetFillStyle(3345)
  bckg.Draw("hist E")
  signal.Draw("same hist E")
  print signal, bckg
  c.SaveAs(filename)

def plotall(fn):
  f = ROOT.TFile(fn)
  print " ### " + fn
  d = f.Get("Method_BDT")
  for k in d.GetListOfKeys():
    k = f.Get(d.GetName() + "/" + k.GetName())
    methodname = k.GetName()
    pathname = "Method_BDT/" + methodname + "/"
    print " # Method_BDT" + methodname
    varlist = set()
    try:
      os.makedirs(outputdir + "/" + pathname)
    except:
      pass
    # rejBvsS
    p = f.Get(pathname + "MVA_" + k.GetName() + "_rejBvsS")
    auc = (p.Integral() / float(p.GetEntries()) - 0.5) * 100
    t = ROOT.TText(0.1, 0.2, "AUC: {0:0.2f}%".format(auc))
    c = ROOT.TCanvas()
    p.Draw()
    t.Draw()
  
    c.SaveAs(outputdir + "/" + pathname + "/" + p.GetName() + ".png")
    del c
    c = ROOT.TCanvas()
    p1 = f.Get(pathname + "MVA_" + k.GetName() + "_S")
    p2 = f.Get(pathname + "MVA_" + k.GetName() + "_B")
    plotStacked(outputdir + "/" + pathname + "/" + "BS.png", p1, p2)
    for p in [x for x in k.GetListOfKeys() if "MVA" not in x.GetName()]:
      varlist.add(p.GetName().replace("_Signal", "").replace("_Background", ""))
    for var in varlist:
      signal = f.Get(pathname + var + "_Signal")
      bckg = f.Get(pathname + var + "_Background")
      if not signal or not bckg:
        continue
      outfile = outputdir + "/" + pathname + var + ".png"
      plotStacked(outfile, signal, bckg)
     
  f.Close()

for l in sys.argv[1:]:
  outputdir=os.path.abspath(os.path.dirname(l)) + "/plots"
  plotall(l)
