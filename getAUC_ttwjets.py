#!/usr/bin/env python

import sys
import ROOT

f = ROOT.TFile(sys.argv[1])

print sys.argv[1].split("/")[1],  "{0:3.2f} {1:3.2f}".format(f.Get("ROC_TT_TuneCUETP8M1_13TeV-powheg-pythia8_ext").Integral() - 50, f.Get("ROC_WJetsToLNu_TuneCUETP8M1_13TeV-madgraphMLM-pythia8").Integral() - 50)

