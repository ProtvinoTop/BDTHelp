import os
import sys
import ROOT

basePath = os.path.join(sys.argv[1])
trainsplit = float(sys.argv[2])
testsplit  = 1 - trainsplit


rootFilesTrain=[]
rootFilesTest=[]
for f in os.listdir(basePath):
    if f.endswith(".root"):
        if f.find("train")!=-1:
            rootFilesTrain.append(os.path.join(basePath,f))
        elif f.find("test")!=-1:
            rootFilesTest.append(os.path.join(basePath,f))

print "found: ",len(rootFilesTrain)," (train) & ",len(rootFilesTest)," (test) files in ",basePath

ROOT.gSystem.Load("/afs/cern.ch/user/g/glatyshe/work/public/top/matthias/Diff13/PxlModules/build/internal/TMVA/libTMVA.so")

ROOT.TMVA.Tools.Instance()
fname = "TMVA.root"
f = ROOT.TFile(fname,"RECREATE")
factory = ROOT.TMVA.Factory("test",f,"!V:Color=False:AnalysisType=Classification:DrawProgressBar=True")

backgroundTrainChains=[]
backgroundTestChains=[]

signalTrainChains=[]
signalTestChains=[]

#     isoQCD = 4253
# antiisoQCD = 353797
# scale = 4253 / 353797 = 0.012021


for bkg in [x.strip() for x in open('background.txt').readlines() if not x.strip().startswith('#') and not x.strip() == ""]:
    background = bkg.split(" ")[0]
    try:
      bw = float(bkg.split(" ")[1])
    except:
      bw = 1
    for rootFile in rootFilesTrain:
        f = ROOT.TFile(rootFile)
        if background in [x.GetName() for x in f.GetListOfKeys()]:
          if f.Get(background).GetEntries() == 0:
            continue
          chainTrain = ROOT.TChain(background,"")
          backgroundTrainChains.append(chainTrain)
          chainTrain.AddFile(rootFile)
          print "Chain", background, "from file", rootFile, "has been added as BACKGROUND"
          if "QCD" in background:
            factory.AddBackgroundTree(chainTrain,1.0/0.5 * bw, ROOT.TMVA.Types.kTraining)
          else:
            factory.AddBackgroundTree(chainTrain,1.0/trainsplit * bw, ROOT.TMVA.Types.kTraining)
        f.Close()

    for rootFile in rootFilesTest:
        f = ROOT.TFile(rootFile)
        if background in [x.GetName() for x in f.GetListOfKeys()]:
          if f.Get(background).GetEntries() == 0:
            continue
          chainTest = ROOT.TChain(background,"")
          backgroundTestChains.append(chainTest)
          chainTest.AddFile(rootFile)
          print "Chain", background, "from file", rootFile, "has been added as BACKGROUND"
          if "QCD" in background:
            factory.AddBackgroundTree(chainTest,1.0/0.5 * bw, ROOT.TMVA.Types.kTesting)
          else:
            factory.AddBackgroundTree(chainTest,1.0/testsplit * bw, ROOT.TMVA.Types.kTesting)
        f.Close()
  
for sgnl in [x.strip() for x in open('signal.txt').readlines() if not x.strip().startswith('#') and not x.strip() == ""]:
    signal = sgnl.split(" ")[0]
    try:
      bw = float(sgnl.split(" ")[1])
    except:
      bw = 1
    for rootFile in rootFilesTrain:
        f = ROOT.TFile(rootFile)
        if signal in [x.GetName() for x in f.GetListOfKeys()]:
          if f.Get(signal).GetEntries() == 0:
            continue
          chainTrain = ROOT.TChain(signal)
          signalTrainChains.append(chainTrain)
          chainTrain.AddFile(rootFile)
          print "Chain", signal, "from file", rootFile, "has been added as SIGNAL"
          factory.AddSignalTree(chainTrain,1.0/trainsplit * bw, ROOT.TMVA.Types.kTraining)
        f.Close()
    
    for rootFile in rootFilesTest:
        f = ROOT.TFile(rootFile)
        if signal in [x.GetName() for x in f.GetListOfKeys()]:
          if f.Get(signal).GetEntries() == 0:
            continue
          chainTest = ROOT.TChain(signal)
          signalTestChains.append(chainTest)
          chainTest.AddFile(rootFile)
          print "Chain", signal, "from file", rootFile, "has been added as SIGNAL"
          factory.AddSignalTree(chainTest,1.0/testsplit * bw, ROOT.TMVA.Types.kTesting)
        f.Close()

factory.SetSignalWeightExpression("100.0*mc_weight*(Reconstructed_1__nSelectedJet==2)*(Reconstructed_1__nSelectedBJet==1)*((Generated_1__genweight<0)*(-1)+(Generated_1__genweight>0)*1)")
factory.SetBackgroundWeightExpression("100.0*mc_weight*(Reconstructed_1__nSelectedJet==2)*(Reconstructed_1__nSelectedBJet==1)*((Generated_1__genweight<0)*(-1)+(Generated_1__genweight>0)*1)")

for line in open("variables.txt", 'r').readlines():
  if not line.startswith("#") and line.strip() != "":
    print "Adding variable '{0}'".format(line.strip())
    factory.AddVariable(line.strip())

for line in open("spectators.txt", 'r').readlines():
  if not line.startswith("#") and line.strip() != "":
    print "Adding spectator '{0}'".format(line.strip())
    factory.AddSpectator(line.strip())

cut = ROOT.TCut("(Reconstructed_1__nSelectedJet==2)*(Reconstructed_1__nSelectedBJet==1)")
factory.PrepareTrainingAndTestTree(cut,"")

for line in open("BDT_methods.txt", 'r').readlines():
  if not line.startswith("#") and line.strip() != "":
    print "Adding method '{0}'".format(line.strip())
    factory.BookMethod(ROOT.TMVA.Types.kBDT, line.strip().split()[0], line.strip().split()[1])

print "THIS IS SPARTA-A!"

factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()



