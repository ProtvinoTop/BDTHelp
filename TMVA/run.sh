#!/bin/bash 

QUEUE=8nh

CMSSW_BASE=/afs/cern.ch/user/g/glatyshe/work/public/top/matthias/Diff13/CMSSW/CMSSW_7_4_15
DIFF13_BASE=/afs/cern.ch/user/g/glatyshe/work/public/top/matthias/Diff13
WORKDIR=$(cd $(dirname $0); pwd -P)

[ -z "$1" ] && TESTPERC=0.3 || TESTPERC=$1
echo "$TESTPERC will be used for training"

for m in $(cat BDT_methods.txt | awk '{print $1}' | sort | uniq); do
  test -z "$m" && continue
  echo $m | grep -qE -- '^ *#' && continue
  mkdir -p $m
  cd $m
  cp -vf ../TMVATraining.py ./
  cp -vf ../variables.txt ./
  cp -vf ../signal.txt ./
  cp -vf ../background.txt ./
  cp -vf ../spectators.txt ./
  cat ../BDT_methods.txt | grep $m > BDT_methods.txt
  cat <<EOF > bsub.sh
#!/bin/bash

cd $DIFF13_BASE
source cmssw.env.sh
source pxl.env.sh
cd $WORKDIR/$m
touch .lock
ionice -c 3 python TMVATraining.py /afs/cern.ch/user/g/glatyshe/work/public/top/matthias/output_root.all/01_09 $TESTPERC &>stdout.log && rm -rf .lock
EOF
  chmod +x bsub.sh
  screen -dmS $m bash bsub.sh
  #bsub -q $QUEUE bsub.sh
  cd - 1> /dev/null 
done
