#!/usr/bin/env python

import sys
import math
import ROOT

def printCorr(th2):
  res = []
  for x in xrange(1, th2.GetNbinsX() + 1):
    varname = th2.GetXaxis().GetBinLabel(x)
    s = 0
    for y in xrange(th2.GetNbinsY()):
      s += (th2.GetBinContent(x, y) / 100.0) ** 2
    res.append((varname, math.sqrt(s)))
  return sorted(res, key=lambda x: x[1])


f = ROOT.TFile(sys.argv[1], "r")
sc = f.Get("CorrelationMatrixS")
bc = f.Get("CorrelationMatrixB")


ress = printCorr(sc)
resb = printCorr(bc)

print "{0:60s} | Signal | Background |".format("Variable")
for key in resb:
  bval = key[1]
  key = key[0]
  sval = [x[1] for x in ress if x[0] == key][0]
  print "{0:60s} | {1:2.4f} | {2:2.4f}     |".format(key, sval, bval)

f.Close()


