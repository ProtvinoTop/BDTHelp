#!/usr/bin/env python

import sys

var = {}

for l in sys.stdin.readlines():
  l = l.strip()
  if len(l.split(":")) != 4 or "Factory" in l or "Variable" in l:
    continue
  v = l.split(":")[2]
  val = l.split(":")[3]
  try:
    val = float(val)
  except:
    continue
  if v not in var:
    var[v] = val
  else:
    var[v] = var[v] + val

vart = []
for k in var.keys():
  vart.append((k, var[k]))

for k, val in sorted(vart, key=lambda x: x[1], reverse=True):
  print k, val
