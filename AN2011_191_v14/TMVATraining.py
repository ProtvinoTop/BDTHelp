import os
import sys
import ROOT

basePath = os.path.join("/afs/cern.ch/user/g/glatyshe/work/public/top/matthias/output_root")

rootFilesTrain=[]
rootFilesTest=[]
for f in os.listdir(basePath):
    if f.endswith(".root"):
        if f.find("train")!=-1:
            rootFilesTrain.append(os.path.join(basePath,f))
        elif f.find("test")!=-1:
            rootFilesTest.append(os.path.join(basePath,f))
       
print "found: ",len(rootFilesTrain)," (train) & ",len(rootFilesTest)," (test) files in ",basePath

ROOT.gSystem.Load("/afs/cern.ch/user/g/glatyshe/work/public/top/matthias/Diff13/PxlModules/build/internal/TMVA/libTMVA.so")

ROOT.TMVA.Tools.Instance()
fname = "test.root" if len(sys.argv) == 1 else sys.argv[1]
f = ROOT.TFile(fname,"RECREATE")
factory = ROOT.TMVA.Factory("test",f,"!V:Color=False:AnalysisType=Classification")

backgroundTrainChains=[]
backgroundTestChains=[]

signalTrainChains=[]
signalTestChains=[]

for background in [
    "TT_TuneCUETP8M1_13TeV-powheg-pythia8_ext",
#    "ST_tW_top_5f_inclusiveDecays_13TeV-powheg-pythia8_TuneCUETP8M1",
    "ST_tW_antitop_5f_inclusiveDecays_13TeV-powheg-pythia8_TuneCUETP8M1",
    "WJetsToLNu_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8",
    "DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8"
]:
    chainTrain = ROOT.TChain(background,"")
    backgroundTrainChains.append(chainTrain)
    for rootFile in rootFilesTrain:
        chainTrain.AddFile(rootFile)
    factory.AddBackgroundTree(chainTrain,1.0/0.3,ROOT.TMVA.Types.kTraining)
    
    chainTest = ROOT.TChain(background,"")
    backgroundTestChains.append(chainTest)
    for rootFile in rootFilesTest:
        chainTest.AddFile(rootFile)
    factory.AddBackgroundTree(chainTest,1.0/0.7,ROOT.TMVA.Types.kTesting)
        
for signal in [
    "ST_t-channel_4f_leptonDecays_13TeV-amcatnlo-pythia8_TuneCUETP8M1"
]:
    chainTrain = ROOT.TChain(signal)
    signalTrainChains.append(chainTrain)
    for rootFile in rootFilesTrain:
        chainTrain.AddFile(rootFile)
    factory.AddSignalTree(chainTrain,1.0/0.3,ROOT.TMVA.Types.kTraining)
    
    chainTest = ROOT.TChain(signal)
    signalTestChains.append(chainTest)
    for rootFile in rootFilesTest:
        chainTest.AddFile(rootFile)
    factory.AddSignalTree(chainTest,1.0/0.7,ROOT.TMVA.Types.kTesting)

factory.SetSignalWeightExpression("100.0*mc_weight*(Reconstructed_1__nSelectedJet==2)*(Reconstructed_1__nSelectedBJet==1)*((Generated_1__genweight<0)*(-1)+(Generated_1__genweight>0)*1)")
factory.SetBackgroundWeightExpression("100.0*mc_weight*(Reconstructed_1__nSelectedJet==2)*(Reconstructed_1__nSelectedBJet==1)*((Generated_1__genweight<0)*(-1)+(Generated_1__genweight>0)*1)")

for line in open("variables.txt", 'r').readlines():
  if not line.startswith("#") and line.strip() != "":
    print "Adding variable '{0}'".format(line.strip())
    factory.AddVariable(line.strip())

cut = ROOT.TCut("(Reconstructed_1__nSelectedJet==2)*(Reconstructed_1__nSelectedBJet==1)")
factory.PrepareTrainingAndTestTree(cut,"")

for line in open("BDT_methods.txt", 'r').readlines():
  if not line.startswith("#") and line.strip() != "":
    print "Adding method '{0}'".format(line.strip())
    factory.BookMethod(ROOT.TMVA.Types.kBDT, line.strip().split()[0], line.strip().split()[1])

factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()



