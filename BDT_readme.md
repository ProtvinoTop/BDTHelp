How to train and evaluate TMVA
==============================

<pre>
# ROOT
cd $BASEDIR/CMSSW/CMSSW_7*
cmsenv

cd $BASEDIR/PxlModules/scripts
vim TMVATraining.py
 # change basePath and ROOT.gSystem.Load path
 # plays with variables and TMVA settings

python TMVATraining.py
</pre>