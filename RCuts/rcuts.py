#!/usr/bin/env python

# time ./rcuts.py ~/work/public/top/matthias/output_root/30_70/



import sys
import os
#import copy
import ROOT

def getW(name, t):
  return [x[1] for x in t if x[0] == name][0]

def treefilter(treeset, filters, variables):
  def check(tree, filtlist):
    for f in filtlist:
      if not eval(f):
        return False
    return True
  result = list()
  filtlist = filters[:]
  npassed = 0
  ndropped = 0
  S = 0
  B = 0
  for v in variables:
    for fid in xrange(len(filtlist)):
      filtlist[fid] = filtlist[fid].replace(v, "getattr(tree,\"{0}\")".format(v))
  for tree, weight in treeset:
    print "Filtering tree", str(tree)
    tree.SetBranchStatus("*", 0)
    for v in variables:
      tree.SetBranchStatus(v, 1)
    tree.SetBranchStatus("mc_weight", 1)
    result.append((tree.CloneTree(0), weight))
    entries = tree.GetEntries()
    getentry = tree.GetEntry
    fill = result[-1][0].Fill
    for entryid in xrange(entries):
      nb = getentry(entryid)
      if nb <= 0:
        print "Reading entry", entryid, "failed"
        continue
      if not check(tree, filtlist):
        B += tree.mc_weight * weight
        ndropped += 1
        continue
      fill()
      S += tree.mc_weight * weight
      npassed += 1
    result[-1][0].AutoSave()
  return (result, S, B, npassed, ndropped)

binning = {
 "SingleTop_1__mtw_beforePz" : (0, 300),
 "SingleTop_1__Top_1__Mass"  : (0, 600),
 "SingleTop_1__TightMuon_BJet_absdEta" : (0, 5),
 "SingleTop_1__LightJet_absEta" : (0, 9),
 "SingleTop_1__BJet_LightJet_dR" : (0, 9),
 "Reconstructed_1__nSelectedJet" : (0, 10),
 "Reconstructed_1__nSelectedBJet" : (0, 10) 
}

# variables
variables = set()
with open("variables.txt", "r") as f:
  for l in f.readlines():
    variables.add(l.strip())

# signal and background
signal = set()
background = set()
signalfiles = set()
backgroundfiles = set()
with open("signal.txt", "r") as fs:
  with open("background.txt", "r") as fb:
    for l in fs.readlines():
      signal.add((l.strip().split()[0], float(l.strip().split()[1])))
    for l in fb.readlines():
      background.add((l.strip().split()[0], float(l.strip().split()[1])))

# filters
filters = list()
with open("filters.txt", "r") as f:
  for l in f.readlines():
    filters.append(l.strip())

print "Starting ..."

# ROOT files location
datapath = "." if len(sys.argv) == 1 else sys.argv[1]
rootfiles_bkg = list()
rootfiles_sig = list()
rootfiles = set()
for f in os.listdir(datapath):
  if f.endswith(".root"):
    fh = ROOT.TFile(os.path.join(datapath, f), 'readonly')
    branch = [k.GetName() for k in fh.GetListOfKeys()][0]
    if branch in [x[0] for x in signal]:
      rootfiles_sig.append((fh.Get(branch), getW(branch, signal)))
      print branch
    elif branch in [x[0] for x in background]:
      rootfiles_bkg.append((fh.Get(branch), getW(branch, background)))
      print branch
    rootfiles.add(fh)

print "Found {0} signal and {1} background root files".format(len(rootfiles_sig), len(rootfiles_bkg))

outfile = ROOT.TFile("output.root", "recreate")
outfile.cd()
histos = {"Sig" : {}, "Bkg" : {}}
for n in ("Sig", "Bkg"):
  for v in variables:
    if v not in binning:
      binning[v] = (0, 300)
    histos[n][v] = ROOT.TH1D(v + "_" + n, v + "_" + n, 250, binning[v][0], binning[v][1])

cutflow_sig = ROOT.TH1I("cutflow_Sig", "cutflow_Sig", 3, 0, 3)
cutflow_bkg = ROOT.TH1I("cutflow_Bkg", "cutflow_Bkg", 3, 0, 3)
cutflow_sig.SetBit(ROOT.TH1.kCanRebin)
cutflow_bkg.SetBit(ROOT.TH1.kCanRebin)

print "=== SIGNAL (ALL FILTERS) ==="
npassed = sum([x[0].GetEntries() for x in rootfiles_sig])
cutflow_sig.Fill("No cuts", npassed)
for f in filters:
  print "Cut:", f
  rootfiles_sig, S, B, npassed, ndropped = treefilter(rootfiles_sig, [f], variables)
  cutflow_sig.Fill(f, npassed)
  SB = S / B if B != 0 else None
  print "S={0} B={1} S/B={2} S/(S+B)={3}".format(S, B, SB, S / (S + B))

print "=== BACKGROUND (ALL FILTERS) ==="
npassed = sum([x[0].GetEntries() for x in rootfiles_bkg])
cutflow_bkg.Fill("No cuts", npassed)
for f in filters:
  print "Cut:", f
  rootfiles_bkg, S, B, npassed, ndropped = treefilter(rootfiles_bkg, [f], variables)
  cutflow_bkg.Fill(f, npassed)
  SB = S / B if B != 0 else None
  print "S={0} B={1} S/B={2} S/(S+B)={3}".format(S, B, SB, S / (S + B))

for sigbr, weight in rootfiles_sig:
  print sigbr.GetName()
  entries = sigbr.GetEntries()
  getentry = sigbr.GetEntry
  for entryid in xrange(entries):
    getentry(entryid)
    for v in variables:
      histos["Sig"][v].Fill(getattr(sigbr, v), weight)

for bkgbr, weight in rootfiles_bkg:
  print bkgbr.GetName()
  entries = bkgbr.GetEntries()
  getentry = bkgbr.GetEntry
  for entryid in xrange(bkgbr.GetEntries()):
    getentry(entryid)
    for v in variables:
      histos["Bkg"][v].Fill(getattr(bkgbr, v), weight)

for n in ("Sig", "Bkg"):
  for v in variables:
    histos[n][v].Write()
cutflow_sig.Write()
cutflow_bkg.Write()
outfile.Close()

for x in rootfiles:
  x.Close()
