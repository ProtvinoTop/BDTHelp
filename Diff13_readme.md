How to work
===========

1. Think as much as possible.

Install Diff13/PxlModules from https://github.com/matt-komm/Diff13
-------

<pre>
git clone https://github.com/matt-komm/Diff13
cd Diff13
git checkout CMSSW_7415

export BASEDIR=`pwd`
source CMSSW/setupCMSSW.sh
cd $BASEDIR

# keep only external/setupCMake.sh external/setupSWIG.sh external/setupPXL.sh uncommented
vim setup.sh
source setup.sh


source setVars.sh pxl-vars.sh
cd PxlModules
mkdir build; cd build
cmake ..
make -j `grep -c proc /proc/cpuinfo` install

#test
cd $BASEDIR
source setVars.sh pxl-vars.sh
python -c "import pxl"
</pre>

How to Update PxlModules
-------

<pre>
cd $BASEDIR
git pull
cd PxlModules/build; cmake ..; make -j `grep -c proc /proc/cpuinfo` install
</pre

How to install VISPA
-------------

<pre>
cd $BASEDIR
wget -O vispa.tar.gz https://forge.physik.rwth-aachen.de/attachments/download/199/vispa-0.6.1.tar.gz
tar xzf vispa.tar.gz
</pre>

How to Run VISPA
---------

<pre>
cd $BASEDIR
source setVars pxl-vars.sh
source /afs/cern.ch/sw/lcg/contrib/gcc/4.9/x86_64-slc6/setup.sh
vispa-0.6.1/bin/vispa
</pre>

How to run pxlrun
-----------------

<pre>
cd $BASEDIR
source setVars.sh pxl-vars.sh
which pxlrun  # test
pxlrun <vispa-xml-output-file>
</pre>