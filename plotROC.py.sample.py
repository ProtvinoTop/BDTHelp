#!/usr/bin/python

import ROOT
import numpy
import random
import math
import os
import re
from optparse import OptionParser
parser = OptionParser()
(options, args) = parser.parse_args()

ROOT.gROOT.Reset()
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptFit(0)
ROOT.gROOT.Reset()
ROOT.gROOT.SetStyle("Plain")
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptFit(1111)
ROOT.gStyle.SetPadTopMargin(0.08)
ROOT.gStyle.SetPadLeftMargin(0.13)
ROOT.gStyle.SetPadRightMargin(0.26)
ROOT.gStyle.SetPadBottomMargin(0.15)
ROOT.gStyle.SetMarkerSize(0.16)
ROOT.gStyle.SetHistLineWidth(1)
ROOT.gStyle.SetStatFontSize(0.025)
ROOT.gStyle.SetLabelSize(0.055, "XYZ")

ROOT.gStyle.SetOptFit()
ROOT.gStyle.SetOptStat(0)

# For the canvas:
ROOT.gStyle.SetCanvasBorderMode(0)
ROOT.gStyle.SetCanvasColor(ROOT.kWhite)
ROOT.gStyle.SetCanvasDefH(600) #Height of canvas
ROOT.gStyle.SetCanvasDefW(600) #Width of canvas
ROOT.gStyle.SetCanvasDefX(0)   #POsition on screen
ROOT.gStyle.SetCanvasDefY(0)

# For the Pad:
ROOT.gStyle.SetPadBorderMode(0)
# ROOT.gStyle.SetPadBorderSize(Width_t size = 1)
ROOT.gStyle.SetPadColor(ROOT.kWhite)
#ROOT.gStyle.SetPadGridX(True)
#ROOT.gStyle.SetPadGridY(True)
ROOT.gStyle.SetGridColor(ROOT.kBlack)
ROOT.gStyle.SetGridStyle(2)
ROOT.gStyle.SetGridWidth(1)

# For the frame:

ROOT.gStyle.SetFrameBorderMode(0)
ROOT.gStyle.SetFrameBorderSize(0)
ROOT.gStyle.SetFrameFillColor(0)
ROOT.gStyle.SetFrameFillStyle(0)
ROOT.gStyle.SetFrameLineColor(1)
ROOT.gStyle.SetFrameLineStyle(1)
ROOT.gStyle.SetFrameLineWidth(0)

# For the histo:
# ROOT.gStyle.SetHistFillColor(1)
# ROOT.gStyle.SetHistFillStyle(0)
ROOT.gStyle.SetHistLineColor(1)
ROOT.gStyle.SetHistLineStyle(0)
ROOT.gStyle.SetHistLineWidth(1)
# ROOT.gStyle.SetLegoInnerR(Float_t rad = 0.5)
# ROOT.gStyle.SetNumberContours(Int_t number = 20)

ROOT.gStyle.SetEndErrorSize(2)
#ROOT.gStyle.SetErrorMarker(20)
ROOT.gStyle.SetErrorX(0.)

ROOT.gStyle.SetMarkerStyle(20)
#ROOT.gStyle.SetMarkerStyle(20)

#For the fit/function:
ROOT.gStyle.SetOptFit(1)
ROOT.gStyle.SetFitFormat("5.4g")
ROOT.gStyle.SetFuncColor(2)
ROOT.gStyle.SetFuncStyle(1)
ROOT.gStyle.SetFuncWidth(1)

#For the date:
ROOT.gStyle.SetOptDate(0)
# ROOT.gStyle.SetDateX(Float_t x = 0.01)
# ROOT.gStyle.SetDateY(Float_t y = 0.01)

# For the statistics box:
ROOT.gStyle.SetOptFile(0)
ROOT.gStyle.SetOptStat(0) # To display the mean and RMS:   SetOptStat("mr")
ROOT.gStyle.SetStatColor(ROOT.kWhite)
ROOT.gStyle.SetStatFont(42)
ROOT.gStyle.SetStatFontSize(0.025)
ROOT.gStyle.SetStatTextColor(1)
ROOT.gStyle.SetStatFormat("6.4g")
ROOT.gStyle.SetStatBorderSize(1)
ROOT.gStyle.SetStatH(0.1)
ROOT.gStyle.SetStatW(0.15)

ROOT.gStyle.SetHatchesSpacing(0.5)
ROOT.gStyle.SetHatchesLineWidth(2)

# ROOT.gStyle.SetStaROOT.TStyle(Style_t style = 1001)
# ROOT.gStyle.SetStatX(Float_t x = 0)
# ROOT.gStyle.SetStatY(Float_t y = 0)


#ROOT.gROOT.ForceStyle(True)
#end modified

# For the Global title:

ROOT.gStyle.SetOptTitle(0)

# ROOT.gStyle.SetTitleH(0) # Set the height of the title box
# ROOT.gStyle.SetTitleW(0) # Set the width of the title box
#ROOT.gStyle.SetTitleX(0.35) # Set the position of the title box
#ROOT.gStyle.SetTitleY(0.986) # Set the position of the title box
# ROOT.gStyle.SetTitleStyle(Style_t style = 1001)
#ROOT.gStyle.SetTitleBorderSize(0)

# For the axis titles:
ROOT.gStyle.SetTitleColor(1, "XYZ")
ROOT.gStyle.SetTitleFont(43, "XYZ")
ROOT.gStyle.SetTitleSize(32, "XYZ")
# ROOT.gStyle.SetTitleXSize(Float_t size = 0.02) # Another way to set the size?
# ROOT.gStyle.SetTitleYSize(Float_t size = 0.02)
ROOT.gStyle.SetTitleXOffset(1.135)
#ROOT.gStyle.SetTitleYOffset(1.2)
ROOT.gStyle.SetTitleOffset(1.32, "YZ") # Another way to set the Offset

# For the axis labels:

ROOT.gStyle.SetLabelColor(1, "XYZ")
ROOT.gStyle.SetLabelFont(43, "XYZ")
ROOT.gStyle.SetLabelOffset(0.0077, "XYZ")
ROOT.gStyle.SetLabelSize(28, "XYZ")
#ROOT.gStyle.SetLabelSize(0.04, "XYZ")

# For the axis:

ROOT.gStyle.SetAxisColor(1, "XYZ")
ROOT.gStyle.SetAxisColor(1, "XYZ")
ROOT.gStyle.SetStripDecimals(True)
ROOT.gStyle.SetTickLength(0.03, "Y")
ROOT.gStyle.SetTickLength(0.05, "X")
ROOT.gStyle.SetNdivisions(505, "X")
ROOT.gStyle.SetNdivisions(512, "Y")

ROOT.gStyle.SetPadTickX(1)  # To get tick marks on the opposite side of the frame
ROOT.gStyle.SetPadTickY(1)

# Change for log plots:
ROOT.gStyle.SetOptLogx(0)
ROOT.gStyle.SetOptLogy(0)
ROOT.gStyle.SetOptLogz(0)

#ROOT.gStyle.SetPalette(1) #(1,0)

# another top group addition
ROOT.gStyle.SetHatchesSpacing(1.0)
ROOT.gStyle.SetHatchesLineWidth(2)

# Postscript options:
#ROOT.gStyle.SetPaperSize(20., 20.)
#ROOT.gStyle.SetPaperSize(ROOT.TStyle.kA4)
#ROOT.gStyle.SetPaperSize(27., 29.7)
#ROOT.gStyle.SetPaperSize(27., 29.7)
ROOT.TGaxis.SetMaxDigits(3)
ROOT.gStyle.SetLineScalePS(2)

# ROOT.gStyle.SetLineStyleString(Int_t i, const char* text)
# ROOT.gStyle.SetHeaderPS(const char* header)
# ROOT.gStyle.SetTitlePS(const char* pstitle)
#ROOT.gStyle.SetColorModelPS(1)

# ROOT.gStyle.SetBarOffset(Float_t baroff = 0.5)
# ROOT.gStyle.SetBarWidth(Float_t barwidth = 0.5)
# ROOT.gStyle.SetPaintTextFormat(const char* format = "g")
# ROOT.gStyle.SetPalette(Int_t ncolors = 0, Int_t* colors = 0)
# ROOT.gStyle.SetTimeOffset(Double_t toffset)
# ROOT.gStyle.SetHistMinimumZero(kTRUE)
ROOT.gStyle.SetPalette(1)
ROOT.gStyle.SetPaintTextFormat("7.4f")



colors=[]
def newColor(red,green,blue):
    newColor.colorindex+=1
    color=ROOT.TColor(newColor.colorindex,red,green,blue)
    colors.append(color)
    return color
    
newColor.colorindex=301

def getDarkerColor(color):
    darkerColor=newColor(color.GetRed()*0.6,color.GetGreen()*0.6,color.GetBlue()*0.6)
    return darkerColor

sampleDict = {

    "tChannel":
    {
        "processes":[
            "ST_t-channel_4f_leptonDecays_13TeV-amcatnlo-pythia8_TuneCUETP8M1",
        ],
        "color":ROOT.gROOT.GetColor(ROOT.kRed),
        "title":"t-channel",
        "weight":"((Generated_1__genweight<0)*(-1)+(Generated_1__genweight>0)*1)"
    },

    "TTJets":
    {
        "processes":[
            "TT_TuneCUETP8M1_13TeV-powheg-pythia8_ext"
        ],
        "color":ROOT.gROOT.GetColor(ROOT.kOrange-3),
        "title":"t#bar{t}",
        "weight":"((Generated_1__genweight<0)*(-1)+(Generated_1__genweight>0)*1)"
    },

    
    "WJets":
    {
        "processes":[
            "WJetsToLNu_TuneCUETP8M1_13TeV-madgraphMLM-pythia8"
        ],
        "color":ROOT.gROOT.GetColor(ROOT.kGreen-2),
        "title":"W+jets",
        "addtitle":"(MadGraph)",
        "weight":"((Generated_1__genweight<0)*(-1)+(Generated_1__genweight>0)*1)"
    },
    
    "QCD":
    {
        "processes":[
            "QCD_Pt-20toInf_MuEnrichedPt15_TuneCUETP8M1_13TeV_pythia8",
        ],
        "weight":"0.015"
    },

}

rootFilesTraining=[]
rootFilesTesting=[]

basedirMC = "/afs/cern.ch/user/g/glatyshe/work/public/top/matthias/output_root/30_70"

matchTraining = re.compile(".*training.*.root")
matchTesting = re.compile(".*testing.*.root")

for f in os.listdir(basedirMC):
    if matchTraining.match(f):
        rootFilesTraining.append(os.path.join(basedirMC,f))
    elif matchTesting.match(f):
        rootFilesTesting.append(os.path.join(basedirMC,f))
        
print "found training=",len(rootFilesTraining)," and testing=",len(rootFilesTesting)," files"

def addUnderflowOverflow(hist):
    hist.SetBinContent(1,hist.GetBinContent(0)+hist.GetBinContent(1))
    N=hist.GetNbinsX()
    hist.SetBinContent(N,hist.GetBinContent(N)+hist.GetBinContent(N+1))
    hist.SetBinContent(0,0)
    hist.SetBinContent(N+1,0)
    hist.SetEntries(hist.GetEntries()-4)
    
def getROC(signal,background):
    N=signal.GetNbinsX()+2
    sigHistContent=numpy.zeros(N)
    bgHistContent=numpy.zeros(N)
    
    for i in range(N):
        sigHistContent[i]=signal.GetBinContent(i)
        bgHistContent[i]=background.GetBinContent(i)

    sigN=sum(sigHistContent)
    bgN=sum(bgHistContent)

    sigEff=[]
    bgRej=[]
    for ibin in range(N):
        sig_integral=0.0
        bg_integral=0.0
        for jbin in range(ibin,N):
            sig_integral+=sigHistContent[jbin]
            bg_integral+=bgHistContent[jbin]
        sigEff.append(sig_integral/sigN)
        bgRej.append(1-bg_integral/bgN)
    
    return sigEff,bgRej
    
def drawROC(name,sigEff,bgRej):

    cv = ROOT.TCanvas("cv_roc"+str(random.random()),"",800,600)
    cv.SetPad(0.0, 0.0, 1.0, 1.0)
    cv.SetFillStyle(4000)

    cv.SetBorderMode(0)
    cv.SetGridx(False)
    cv.SetGridy(False)

    #For the frame:
    cv.SetFrameBorderMode(0)
    cv.SetFrameBorderSize(1)
    cv.SetFrameFillColor(0)
    cv.SetFrameFillStyle(0)
    cv.SetFrameLineColor(1)
    cv.SetFrameLineStyle(1)
    cv.SetFrameLineWidth(1)

    # Margins:
    cv.SetLeftMargin(0.14)
    cv.SetRightMargin(0.03)
    cv.SetTopMargin(0.08)
    cv.SetBottomMargin(0.135)

    # For the Global title:
    cv.SetTitle("")

    # For the axis:
    cv.SetTickx(1)  # To get tick marks on the opposite side of the frame
    cv.SetTicky(1)

    axis=ROOT.TH2F("axis"+str(random.random()),";signal efficiency;background rejection",50,0,1.0,50,0.0,1.0)
    axis.GetYaxis().SetNdivisions(508)
    axis.GetXaxis().SetNdivisions(508)
    axis.GetXaxis().SetTickLength(0.015/(1-cv.GetLeftMargin()-cv.GetRightMargin()))
    axis.GetYaxis().SetTickLength(0.015/(1-cv.GetTopMargin()-cv.GetBottomMargin()))
    axis.GetYaxis().SetNoExponent(True)
    axis.Draw("AXIS")

    #### draw here
    graphF = ROOT.TGraph(len(sigEff),numpy.array(sigEff),numpy.array(bgRej))
    graphF.SetLineWidth(0)
    graphF.SetFillColor(ROOT.kAzure-4)
    graphF.Draw("SameF")

    graphL = ROOT.TGraph(len(sigEff),numpy.array(sigEff),numpy.array(bgRej))
    graphL.SetLineColor(ROOT.kAzure-5)
    graphL.SetLineWidth(2)
    graphL.Draw("SameL")

    ROOT.gPad.RedrawAxis()
    
    pCMS=ROOT.TPaveText(cv.GetLeftMargin(),0.94,cv.GetLeftMargin(),0.94,"NDC")
    pCMS.SetFillColor(ROOT.kWhite)
    pCMS.SetBorderSize(0)
    pCMS.SetTextFont(63)
    pCMS.SetTextSize(30)
    pCMS.SetTextAlign(11)
    pCMS.AddText("CMS")
    pCMS.Draw("Same")

    pPreliminary=ROOT.TPaveText(cv.GetLeftMargin()+0.095,0.94,cv.GetLeftMargin()+0.095,0.94,"NDC")
    pPreliminary.SetFillColor(ROOT.kWhite)
    pPreliminary.SetBorderSize(0)
    pPreliminary.SetTextFont(53)
    pPreliminary.SetTextSize(30)
    pPreliminary.SetTextAlign(11)
    pPreliminary.AddText("Simulation")
    pPreliminary.Draw("Same")
    
    
    pAUC=ROOT.TPaveText(1-cv.GetRightMargin(),0.94,1-cv.GetRightMargin(),0.94,"NDC")
    pAUC.SetFillColor(ROOT.kWhite)
    pAUC.SetBorderSize(0)
    pAUC.SetTextFont(43)
    pAUC.SetTextSize(24)
    pAUC.SetTextAlign(31)
    pAUC.AddText("AUC: % 4.1f %%" % (getAUC(sigEff,bgRej)*100.0))
    pAUC.Draw("Same")

    cv.Update()
    cv.Print(name+".pdf")
    cv.Print(name+".png")
    cv.WaitPrimitive()
    
def getAUC(sigEff,bgRej):
    integral=0.0
    for i in range(len(sigEff)-1):
        w=math.fabs(sigEff[i+1]-sigEff[i])
        h=0.5*(bgRej[i+1]+bgRej[i])
        x=(sigEff[i+1]+sigEff[i])*0.5
        integral+=w*math.fabs(h-(1-x))
    return math.fabs(integral)
        



globalMCWeight="(Reconstructed_1__HLT_IsoMu20_v1)*mc_weight*((Generated_1__genweight<0)*(-1)+(Generated_1__genweight>0)*1)"

binning=500

rocCurves=[]

for precuts in [
    ["2j1t","(Reconstructed_1__nSelectedJet==2)*(Reconstructed_1__nSelectedBJet==1)"],
]:
    
    for var in [
#        ["BDT_boost04_minnode001_maxvar3_ntree1000_invboost","Reconstructed_1__BDT_adaboost04_minnode001_maxvar2_ntree600_invboost","BDT discriminant","","1",binning,-1,1],
        ["ljet_abseta","fabs(SingleTop_1__LightJet_1__Eta)","j' |#eta|","","1",binning,0,5.0],
        ["bjet_muon_dEta","abs(SingleTop_1__BJet_TightMuon_dEta)","#Delta #eta (b jet, #mu)","","1",binning,0,5.0],
    
        ["ljet_n60","SingleTop_1__LightJet_1__n60","j' n60","","1",41,-0.5,40.5],
        ["ljet_n90","SingleTop_1__LightJet_1__n90","j' n90","","1",51,-0.5,50.5],
        ["ljet_csv","SingleTop_1__LightJet_1__pfCombinedInclusiveSecondaryVertexV2BJetTags","j' CSV","","1",binning,0,1],
        
        ["dijet_dR","SingleTop_1__Dijet_1__DR","#Delta R(j', b jet)","","1",binning,0,8],
        ["dijet_mass","SingleTop_1__Dijet_1__Mass","dijet mass","GeV","1",binning,0,800],
        
        ["aplanarity","Reconstructed_1__logaplanarity","log aplanarity","","1",binning,-15,0.0],
        ["circularity","Reconstructed_1__circularity","circularity","","1",binning,0,1],
        ["C","Reconstructed_1__C","C","","1",binning,0,1],
        ["isotropy","Reconstructed_1__isotropy","isotropy","","1",binning,0,1],
        ["sphericity","Reconstructed_1__sphericity","sphericity","","1",binning,0,1],
        
        ["top_mass","SingleTop_1__Top_1__Mass","m_{lb#nu}","GeV","1",binning,0,500],
        ["mtw","SingleTop_1__mtw_beforePz","M_{T}(W)","GeV","1",binning,0,250],
        
        ["shat_mass","SingleTop_1__Shat_1__Mass","#hat{s}","GeV","1",binning,0,1500],
        ["shat_pt","SingleTop_1__Shat_1__Pt","#hat{s} p_{T}","GeV","1",binning,0,300],
        
        ["muon_pt","Reconstructed_1__TightMuon_1__Pt","muon p_{T}","GeV","1",binning,0,250],
        ["met","Reconstructed_1__MET_1__Pt","MET","GeV","1",binning,0,250],
    ]:
        '''
        ["aplanarity","log(Reconstructed_1__aplanarity)","log(aplanarity)","","1",binning,-15,0.0],
        ["circularity","Reconstructed_1__circularity","circularity","","1",binning,0,1],
        ["C","Reconstructed_1__C","C","","1",binning,0,1],
        ["isotropy","Reconstructed_1__isotropy","isotropy","","1",binning,0,1],
        ["sphericity","Reconstructed_1__sphericity","sphericity","","1",binning,0,1],

    
        ["BDT_boost02_minnode005_maxvar2_ntree1000_invboost","Reconstructed_1__BDT_boost02_minnode005_maxvar2_ntree1000_invboost","BDT (#beta=0.2, min=5%)","","1",binning,-0.8,0.8],
        ["BDT_boost03_minnode005_maxvar2_ntree1000_invboost","Reconstructed_1__BDT_boost03_minnode005_maxvar2_ntree1000_invboost","BDT (#beta=0.3, min=5%)","","1",binning,-0.8,0.8],
        ["BDT_boost04_minnode005_maxvar2_ntree1000_invboost","Reconstructed_1__BDT_boost04_minnode005_maxvar2_ntree1000_invboost","BDT (#beta=0.4, min=5%)","","1",binning,-0.8,0.8],
        ["BDT_boost05_minnode005_maxvar2_ntree1000_invboost","Reconstructed_1__BDT_boost05_minnode005_maxvar2_ntree1000_invboost","BDT (#beta=0.5, min=5%)","","1",binning,-0.8,0.8],
        
        ["BDT_boost02_minnode005_maxvar2_ntree1000_invboost_nLoose","Reconstructed_1__BDT_boost02_minnode005_maxvar2_ntree1000_invboost_nLoose","BDT (#beta=0.2, min=5%) + # loose","","1",binning,-0.8,0.8],
        ["BDT_boost03_minnode005_maxvar2_ntree1000_invboost_nLoose","Reconstructed_1__BDT_boost03_minnode005_maxvar2_ntree1000_invboost_nLoose","BDT (#beta=0.3, min=5%) + # loose","","1",binning,-0.8,0.8],
        ["BDT_boost04_minnode005_maxvar2_ntree1000_invboost_nLoose","Reconstructed_1__BDT_boost04_minnode005_maxvar2_ntree1000_invboost_nLoose","BDT (#beta=0.4, min=5%) + # loose","","1",binning,-0.8,0.8],
        ["BDT_boost05_minnode005_maxvar2_ntree1000_invboost_nLoose","Reconstructed_1__BDT_boost05_minnode005_maxvar2_ntree1000_invboost_nLoose","BDT (#beta=0.5, min=5%) + # loose","","1",binning,-0.8,0.8],
    
        ["BDT_gradboost02_minnode005_maxvar2_ntree600_invboost","Reconstructed_1__BDT_gradboost02_minnode005_maxvar2_ntree600_invboost","BDT (shrink=0.2, min=5%) + # loose","","1",binning,-1,1],
        ["BDT_gradboost03_minnode005_maxvar2_ntree600_invboost","Reconstructed_1__BDT_gradboost03_minnode005_maxvar2_ntree600_invboost","BDT (shrink=0.3, min=5%) + # loose","","1",binning,-1,1],
        ["BDT_gradboost04_minnode005_maxvar2_ntree600_invboost","Reconstructed_1__BDT_gradboost04_minnode005_maxvar2_ntree600_invboost","BDT (shrink=0.4, min=5%) + # loose","","1",binning,-1,1],
        ["BDT_gradboost05_minnode005_maxvar2_ntree600_invboost","Reconstructed_1__BDT_gradboost05_minnode005_maxvar2_ntree600_invboost","BDT (shrink=0.5, min=5%) + # loose","","1",binning,-1,1],
    
    
        ["ljet_pt","SingleTop_1__LightJet_1__Pt","light jet p_{T}","GeV","1",binning,0,250],
        ["ljet_abseta","fabs(SingleTop_1__LightJet_1__Eta)","light jet |#eta|","","1",binning,0,5.0],
        ["ljet_mass","SingleTop_1__LightJet_1__Mass","light jet mass","GeV","1",binning,0.0,40.0],
        ["ljet_n90","SingleTop_1__LightJet_1__n90","light jet n90","","1",51,-0.5,50.5],
        ["ljet_n60","SingleTop_1__LightJet_1__n60","light jet n60","","1",41,-0.5,40.5],
        ["ljet_csv","SingleTop_1__LightJet_1__pfCombinedInclusiveSecondaryVertexV2BJetTags","light jet CSV","","1",binning,0,1],
        
                
        ["bjet_pt","SingleTop_1__BJet_1__Pt","b-jet p_{T}","GeV","1",binning,0,250],
        ["bjet_abseta","fabs(SingleTop_1__BJet_1__Eta)","b-jet |#eta|","","1",binning,0,2.5],
        ["bjet_mass","SingleTop_1__BJet_1__Mass","b-jet mass","GeV","1",binning,0.0,40.0],
        ["bjet_n90","SingleTop_1__BJet_1__n90","b jet n90","","1",51,-0.5,50.5],
        ["bjet_n60","SingleTop_1__BJet_1__n60","b jet n60","","1",41,-0.5,40.5],
        
        ["dijet_pt","SingleTop_1__Dijet_1__Pt","dijet p_{T}","GeV","1",binning,0,350],
        ["dijet_deltaEta","SingleTop_1__Dijet_1__DEta","#Delta #eta(light jet, b jet)","","1",binning,0,8],
        ["dijet_mass","SingleTop_1__Dijet_1__Mass","dijet mass","GeV","1",binning,0,800],
        
        ["top_pt","SingleTop_1__Top_1__Eta","top #eta","","1",binning,-6,6],
        ["top_eta","SingleTop_1__Top_1__Eta","top #eta","","1",binning,-6,6],
        ["top_mass","SingleTop_1__Top_1__Mass","m_{lb#nu}","GeV","1",binning,0,500],
        
        ["muon_bjet_EtSum","SingleTop_1__sumPt_BJet_TightMuon","E_{#mu}+E_{b jet}","GeV","1",binning,0,600],
        ["muon_pt","Reconstructed_1__TightMuon_1__Pt","muon p_{T}","GeV","1",binning,0,250],
        
        ["met","Reconstructed_1__MET_1__Pt","MET","GeV","1",binning,0,250],
        ["mtw","SingleTop_1__mtw_beforePz","M_{T}(W)","GeV","1",binning,0,250],
        ["muon_met_dphi","SingleTop_1__TightMuon_MET_dPhi","#Delta #phi(#mu, MET)","","1",binning,0,3.2],
        
        ["shat_pt","SingleTop_1__Shat_1__Pt","#hat{s} p_{T}","GeV","1",binning,0,300],
        ["shat_abseta","fabs(SingleTop_1__Shat_1__Eta)","#hat{s} |#eta|","","1",binning,0,7],
        ["shat_mass","SingleTop_1__Shat_1__Mass","#hat{s}","GeV","1",binning,0,1500],
        
        ["nLooseJets","Reconstructed_1__nSelectedLooseJet","# 30<jets<40 GeV","","1",6,-0.5,5.5],
        ["fox_eta_1","Reconstructed_1__fox_1_eta","H_{1}(#eta)","","1",binning,0.0,1.0],
        ["fox_pz_1","Reconstructed_1__fox_1_logpz","log(H_{1}(p_{z}))","","1",binning,-2,5.0],
    
        ["jet_muon_charge","Reconstructed_1__TightMuon_1__Charge*SingleTop_1__BJet_1__jetCharge","Q_{b jet} #upoint Q_{#mu}","","1",binning,-1.5,1.5],
    ]:
        '''
        
        variableName=var[1]
        variableTitle=var[2]
        unit=var[3]
        weight=var[4]+"*"+precuts[1]
        nbins=var[5]
        xmin=var[6]
        xmax=var[7]
        
        print var[0]
        
        sumHistBackgroundTraining = ROOT.TH1F("hist_bg"+str(random.random),"",nbins,xmin,xmax)
        sumHistBackgroundTraining.Sumw2()
        sumHistBackgroundTraining.SetDirectory(0)
        
        sumHistBackgroundTesting = ROOT.TH1F("hist_bg"+str(random.random),"",nbins,xmin,xmax)
        sumHistBackgroundTesting.Sumw2()
        sumHistBackgroundTesting.SetDirectory(0)
        
        for sampleName in ["TTJets","WJets"]:
            sample=sampleDict[sampleName]
            for process in sample["processes"]:
                print "process:", process
                for f in rootFilesTraining:
                    rootFile = ROOT.TFile(f)
                    tree = rootFile.Get(process)
                    if (tree):
                        tempHist=ROOT.TH1F(sumHistBackgroundTraining)
                        tempHist.SetName("hist_"+sampleName+str(random.random))
                        tree.Project(tempHist.GetName(),variableName,sample["weight"]+"*"+weight+"*"+globalMCWeight)
                        tempHist.SetDirectory(0)
                        #print "background: ",process,sumHistBackgroundTraining.GetEntries()
                        addUnderflowOverflow(tempHist)

                        sumHistBackgroundTraining.Add(tempHist)
                    rootFile.Close()
                    #break
                    
                for f in rootFilesTesting:
                    rootFile = ROOT.TFile(f)
                    tree = rootFile.Get(process)
                    if (tree):
                        tempHist=ROOT.TH1F(sumHistBackgroundTesting)
                        tempHist.SetName("hist_"+sampleName+str(random.random))
                        tree.Project(tempHist.GetName(),variableName,sample["weight"]+"*"+weight+"*"+globalMCWeight)
                        tempHist.SetDirectory(0)
                        #print "background: ",process,sumHistBackgroundTesting.GetEntries()
                        addUnderflowOverflow(tempHist)

                        sumHistBackgroundTesting.Add(tempHist)
                    rootFile.Close()
                    #break
        if sumHistBackgroundTraining.Integral() == 0 or sumHistBackgroundTesting.Integral() == 0:
            print "skipping ..."
            continue        
        sumHistBackgroundTraining.Scale(1.0/sumHistBackgroundTraining.Integral())
        sumHistBackgroundTesting.Scale(1.0/sumHistBackgroundTesting.Integral())        
                    
                    
        sumHistSignalTraining = ROOT.TH1F("hist_sig"+str(random.random),"",nbins,xmin,xmax)
        sumHistSignalTraining.Sumw2()
        sumHistSignalTraining.SetDirectory(0)
        
        sumHistSignalTesting = ROOT.TH1F("hist_sig"+str(random.random),"",nbins,xmin,xmax)
        sumHistSignalTesting.Sumw2()
        sumHistSignalTesting.SetDirectory(0)
        
        for sampleName in ["tChannel"]:
            sample=sampleDict[sampleName]
            for process in sample["processes"]:
                for f in rootFilesTraining:
                    rootFile = ROOT.TFile(f)
                    tree = rootFile.Get(process)
                    if (tree):
                        tempHist=ROOT.TH1F(sumHistSignalTraining)
                        tempHist.SetName("hist_"+sampleName+str(random.random))
                        tree.Project(tempHist.GetName(),variableName,sample["weight"]+"*"+weight+"*"+globalMCWeight)
                        tempHist.SetDirectory(0)
                        #print "background: ",process,sumHistSignalTraining.GetEntries()
                        addUnderflowOverflow(tempHist)

                        sumHistSignalTraining.Add(tempHist)
                    rootFile.Close()
                    #break
                    
                for f in rootFilesTesting:
                    rootFile = ROOT.TFile(f)
                    tree = rootFile.Get(process)
                    if (tree):
                        tempHist=ROOT.TH1F(sumHistSignalTesting)
                        tempHist.SetName("hist_"+sampleName+str(random.random))
                        tree.Project(tempHist.GetName(),variableName,sample["weight"]+"*"+weight+"*"+globalMCWeight)
                        tempHist.SetDirectory(0)
                        #print "background: ",process,sumHistBackgroundTesting.GetEntries()
                        addUnderflowOverflow(tempHist)

                        sumHistSignalTesting.Add(tempHist)
                    rootFile.Close()
                    #break
                    
        sumHistSignalTraining.Scale(1.0/sumHistSignalTraining.Integral())
        sumHistSignalTesting.Scale(1.0/sumHistSignalTesting.Integral())
        
        signalEff,backgroundRej = getROC(sumHistSignalTesting,sumHistBackgroundTesting)
        drawROC("roc/"+precuts[0]+"_"+var[0]+"_roc",signalEff,backgroundRej)
        auc = getAUC(signalEff,backgroundRej)
        
        if sumHistSignalTraining.GetNbinsX()>200:
            sumHistSignalTraining.Rebin(10)
            sumHistSignalTesting.Rebin(10)
            
            sumHistBackgroundTraining.Rebin(10)
            sumHistBackgroundTesting.Rebin(10)

        cv = ROOT.TCanvas("cv_dist"+str(random.random()),"",800,600)
        cv.SetPad(0.0, 0.0, 1.0, 1.0)
        cv.SetFillStyle(4000)


        cv.SetBorderMode(0)
        cv.SetGridx(False)
        cv.SetGridy(False)


        #For the frame:
        cv.SetFrameBorderMode(0)
        cv.SetFrameBorderSize(1)
        cv.SetFrameFillColor(0)
        cv.SetFrameFillStyle(0)
        cv.SetFrameLineColor(1)
        cv.SetFrameLineStyle(1)
        cv.SetFrameLineWidth(1)

        # Margins:
        cv.SetLeftMargin(0.14)
        cv.SetRightMargin(0.205)
        cv.SetTopMargin(0.08)
        cv.SetBottomMargin(0.135)

        # For the Global title:
        cv.SetTitle("")

        # For the axis:
        cv.SetTickx(1)  # To get tick marks on the opposite side of the frame
        cv.SetTicky(1)

        axis=ROOT.TH2F("axis"+str(random.random()),";"+variableTitle+";Normalized events",nbins,xmin,xmax,50,0.0,1.2*max(sumHistBackgroundTesting.GetMaximum(),sumHistSignalTesting.GetMaximum()))
        axis.GetYaxis().SetNdivisions(508)
        axis.GetXaxis().SetNdivisions(508)
        axis.GetXaxis().SetTickLength(0.015/(1-cv.GetLeftMargin()-cv.GetRightMargin()))
        axis.GetYaxis().SetTickLength(0.015/(1-cv.GetTopMargin()-cv.GetBottomMargin()))
        axis.GetYaxis().SetNoExponent(True)
        axis.Draw("AXIS")

        #### draw here

        sumHistBackgroundTesting.SetLineColor(ROOT.kAzure-7)
        sumHistBackgroundTesting.SetLineWidth(3)
        sumHistBackgroundTesting.SetFillColor(ROOT.kAzure-8)
        sumHistBackgroundTesting.SetFillStyle(3354)
        sumHistBackgroundTesting.Draw("HIST Same")
        
        sumHistBackgroundTraining.SetLineColor(ROOT.kAzure-7)
        sumHistBackgroundTraining.SetLineWidth(2)
        sumHistBackgroundTraining.SetMarkerColor(ROOT.kAzure-8)
        sumHistBackgroundTraining.SetMarkerStyle(20)
        sumHistBackgroundTraining.SetMarkerSize(0.9)
        sumHistBackgroundTraining.Draw("PE Same")
        
        
        sumHistSignalTesting.SetLineColor(ROOT.kOrange+7)
        sumHistSignalTesting.SetLineWidth(3)
        sumHistSignalTesting.SetFillColor(ROOT.kOrange+1)
        sumHistSignalTesting.SetFillStyle(3345)
        sumHistSignalTesting.Draw("HIST Same")
        
        
        sumHistSignalTraining.SetLineColor(ROOT.kOrange+7)
        sumHistSignalTraining.SetLineWidth(2)
        sumHistSignalTraining.SetMarkerColor(ROOT.kOrange+1)
        sumHistSignalTraining.SetMarkerStyle(20)
        sumHistSignalTraining.SetMarkerSize(0.9)
        sumHistSignalTraining.Draw("PE Same")

        ROOT.gPad.RedrawAxis()

        legend = ROOT.TLegend(0.8,0.9,0.99,0.6)

        legend.SetFillColor(ROOT.kWhite)
        legend.SetBorderSize(0)
        legend.SetTextFont(43)
        legend.SetTextSize(28)
        legend.AddEntry(sumHistSignalTesting,"t-channel","F")
        legend.AddEntry(sumHistSignalTraining,"training","P")
        legend.AddEntry(sumHistBackgroundTesting,"t#bar{t}#kern[-0.5]{ }/#kern[-0.5]{ }W+jets","F")
        legend.AddEntry(sumHistBackgroundTraining,"training","P")
        legend.Draw("Same")
            
        pText=ROOT.TPaveText(cv.GetLeftMargin(),0.94,cv.GetLeftMargin(),0.94,"NDC")
        pText.SetFillColor(ROOT.kWhite)
        pText.SetBorderSize(0)
        pText.SetTextFont(63)
        pText.SetTextSize(26)
        pText.SetTextAlign(11)
        pText.AddText(precuts[0])
        pText.Draw("Same")

        pCMS=ROOT.TPaveText(cv.GetLeftMargin()+0.025,0.85,cv.GetLeftMargin()+0.025,0.85,"NDC")
        pCMS.SetFillColor(ROOT.kWhite)
        pCMS.SetBorderSize(0)
        pCMS.SetTextFont(63)
        pCMS.SetTextSize(30)
        pCMS.SetTextAlign(11)
        pCMS.AddText("CMS")
        pCMS.Draw("Same")

        pPreliminary=ROOT.TPaveText(cv.GetLeftMargin()+0.115,0.85,cv.GetLeftMargin()+0.115,0.85,"NDC")
        pPreliminary.SetFillColor(ROOT.kWhite)
        pPreliminary.SetBorderSize(0)
        pPreliminary.SetTextFont(53)
        pPreliminary.SetTextSize(30)
        pPreliminary.SetTextAlign(11)
        pPreliminary.AddText("Simulation")
        pPreliminary.Draw("Same")
        
        
        pAUC=ROOT.TPaveText(1-cv.GetRightMargin(),0.94,1-cv.GetRightMargin(),0.94,"NDC")
        pAUC.SetFillColor(ROOT.kWhite)
        pAUC.SetBorderSize(0)
        pAUC.SetTextFont(43)
        pAUC.SetTextSize(24)
        pAUC.SetTextAlign(31)
        pAUC.AddText("AUC: %4.1f%%,  KS: S(B)=%4.1f%% (%4.1f%%)" % (100.0*auc,100.0*sumHistSignalTraining.KolmogorovTest(sumHistSignalTesting),100.0*sumHistBackgroundTraining.KolmogorovTest(sumHistBackgroundTesting)))
        pAUC.Draw("Same")
        

        legend.Draw("Same")

        cv.Update()
        cv.Print("roc/"+precuts[0]+"_"+var[0]+".pdf")
        cv.Print("roc/"+precuts[0]+"_"+var[0]+".png")
        cv.WaitPrimitive()

        #break




