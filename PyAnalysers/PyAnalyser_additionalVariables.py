### Skeleton for PyAnalyser_additionalVariables.py
# created by VISPA
# Tue Nov 24 10:43:24 2015
# -*- coding: utf-8 -*-

from pxl import core, modules
import math
from ROOT import TLorentzVector

def getVector(p):
  v = TLorentzVector()
  v.SetPxPyPzE(p.getPx(), p.getPy(), p.getPz(), p.getE())
  return v

def InvariantMass(p1, p2):
  v1 = getVector(p1)
  v2 = getVector(p2)
  v12 = v1+v2
  #raise NameError(type(p1))
  return v12.M()
  
def deltaR(p1, p2):
  v1 = getVector(p1)
  v2 = getVector(p2)
  return v1.DrEtaPhi(v2)
  
def deltaPhi(p1, p2):
  v1 = getVector(p1)
  v2 = getVector(p2)
  return v1.DeltaPhi(v2)

def HT(p1, p2):
  return (p1.getPt() + p2.getPt())
  
def InvariantMass3(p1, p2, p3):
  v1 = getVector(p1)
  v2 = getVector(p2)
  v3 = getVector(p3)
  v123 = v1+v2+v3
  return v123.M()
  
def CosThetaMuW(mu, W):
  mu_b = getVector(mu)
  vW = getVector(W)
  mu_b.Boost(-vW.BoostVector())
  return math.cos(mu_b.Angle(-vW.BoostVector()))
  
def CosThetaMuLJ(mu, lj, top):
  vT = getVector(top)
  mu_b = getVector(mu)
  mu_b.Boost(-vT.BoostVector())
  lj_b = getVector(lj)
  lj_b.Boost(-vT.BoostVector())
  return math.cos(mu_b.Angle(lj_b.Vect()))

def PtJJ(p1, p2):
  ptx = p1.getPx() + p2.getPx()
  pty = p1.getPy() + p2.getPy()
  return math.sqrt(ptx*ptx + pty*pty)

class Example(modules.PythonModule):

    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)

    def initialize(self, module):
        ''' Initialize module options '''
        self.__module = module
        self._logger = core.Logger(self.__module.getName())
        # module.addOption("example option", "example description", "default value")

    def beginJob(self, parameters=None):
        ''' Executed before the first object comes in '''
        self._logger.log(core.LOG_LEVEL_INFO, "Begin job")

    def beginRun(self):
        ''' Executed before each run '''
        pass

    def getEventView(self, event, name):
      try:
        return [ev for ev in event.getEventViews() if ev.getName() == name][0]
      except:
        return None
  
    def getParticle(self, ev, name):
      try:
        return [p for p in ev.getParticles() if p.getName() == name][0]
      except:
        return None

    def analyse(self, object):
        ''' Executed on every object '''
        event = core.toEvent(object)
        ev = self.getEventView(event, "SingleTop")
        ev2 = self.getEventView(event, "Reconstructed")
        ljet = self.getParticle(ev, "LightJet")
        bjet = self.getParticle(ev, "BJet")
        W = self.getParticle(ev, "W")
        lepton = self.getParticle(ev2, "TightMuon")
        top = self.getParticle(ev, "Top")
        ev.setUserRecord("Jet_1_Jet_2_Ht", HT(ljet, bjet))
        ev.setUserRecord("W_Jet_1_Jet_2_invM", InvariantMass3(ljet, bjet, W))
        ev.setUserRecord("Jet_1_Jet_2_Pt", PtJJ(ljet, bjet))
        ev.setUserRecord("W_BJet_invM", InvariantMass(W, bjet))
        ev.setUserRecord("W_Mu_CosTheta", CosThetaMuW(lepton, W))
        ev.setUserRecord("W_LightJet_CosTheta_T", CosThetaWLJ(W, ljet, top))
        ev.setUserRecord("Top_LightJet_Sqrt_S", (getVector(top).Vect() + getVector(ljet).Vect()).Mag())
        particles = (lepton, W, bjet, ljet)
        for p0 in xrange(len(particles)):
          p1 = particles[p0]
          ev.setUserRecord("{0}_absEta".format(p1.getName()), abs(p1.getEta()))
          ev.setUserRecord("{0}_absPhi".format(p1.getName()), abs(p1.getPhi()))
          for p2 in particles[p0 + 1:]:
            if p1 == p2:
              continue
            ev.setUserRecord("{0}_{1}_dR".format(p1.getName(), p2.getName()), deltaR(p1, p2))
            ev.setUserRecord("{0}_{1}_dEta".format(p1.getName(), p2.getName()), p1.getEta() - p2.getEta())
            ev.setUserRecord("{0}_{1}_absdEta".format(p1.getName(), p2.getName()), abs(p1.getEta() - p2.getEta()))
            ev.setUserRecord("{0}_{1}_dPhi".format(p1.getName(), p2.getName()), deltaPhi(p1, p2))
            ev.setUserRecord("{0}_{1}_absdPhi".format(p1.getName(), p2.getName()), abs(deltaPhi(p1, p2)))
            ev.setUserRecord("{0}_{1}_invM".format(p1.getName(), p2.getName()), InvariantMass(p1, p2))

    def endRun(self):
        ''' Executed after each run '''
        pass

    def endJob(self):
        ''' Executed after the last object '''
        self._logger.log(core.LOG_LEVEL_INFO, "End job")
