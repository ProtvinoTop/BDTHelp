#!/usr/bin/env python

import sys
import subprocess
import ROOT

treesets = []
filesets = []

for fn in sys.argv[1:]:
  f = ROOT.TFile(fn)
  tree = f.Get([x.GetName() for x in f.GetListOfKeys()][0])
  branchset = set([x.GetName() for x in tree.GetListOfBranches()])
  if branchset not in treesets:
    treesets.append(branchset)
    filesets.append([])
  idx = treesets.index(branchset)
  filesets[idx].append(fn)
  f.Close()

for l in xrange(len(filesets)):
  flist = filesets[l]
  cmd = ['hadd', 'outfile.' + str(l) + ".root"] + flist
  print " ".join(cmd)
