#!/usr/bin/env python

# Usage:
# cat weights/test_BDT_boost04_minnode001_maxvar3_ntree1000_invboost.weights.xml | python .../bdt2rcuts.py
# see cuts.root
#
# If you want -- change required depth (DT level) in the code (it starts from 0) :)
# e.g., depth == 0 means use only initial DT point.

# And fill ...

import sys
import ROOT

variables = {}
layers = [int(x) for x in sys.argv[1:]]

if len(layers) == 0:
  layers.append(0)

for l in sys.stdin:
  l = l.strip()
  if "pos" not in l and "VarIndex" not in l:
    continue
  keys = {}
  for i in l.split(" ")[1:]:
    i = i.replace("/", "").replace(">", "")
    key, value = i.split("=")
    value = value.replace("\"", "")
    try:
      value = float(value)
    except:
      value = str(value)
    keys[key] = value
  if "<Variable" in l:
    if keys["Expression"] not in variables:
      variables[keys['Expression']] = keys
      variables[keys['Expression']].update({"cuts" : []})
  else:
    varid = keys['IVar']
    if varid == -1 :
      continue
### CHANGE REQUIRED DEPTH HERE ###
    if keys['depth'] not in layers:
      continue
    varname = [ x for x in variables.iterkeys() if variables[x]["VarIndex"] == varid][0]
    variables[varname]["cuts"].append((keys["Cut"], keys["purity"], keys['cType']))
    print "{0:50s} {1} {2:.2f}".format(varname, ["<", ">"][keys['cType'] == 1], keys["Cut"])

print "#" * 80
hs = []
rf = ROOT.TFile("cuts.root", "recreate")
for ctype in (0, 1):
  cname = ("_down", "_up")[ctype == 1]
  for i in variables.keys():
    minval = min([x[0] for x in  variables[i]["cuts"]])
    maxval = max([x[0] for x in  variables[i]["cuts"]])
    h = ROOT.TH1F(i + cname, i + (" < X", " > X")[ctype ==1 ] , 100, minval, maxval)
    hs.append(h)
    for (cut, weight) in [(x[0], x[1]) for x in variables[i]["cuts"] if x[2] == ctype]:
      #h.Fill(cut, weight)
      h.Fill(cut)
    maxbin = h.GetMaximumBin()
    print "MaxBin (", i, ") (", ("<", ">")[ctype == 1] ,") :", h.GetBinCenter(maxbin)
    h.Write()
rf.Close()


