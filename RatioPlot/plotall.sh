#!/bin/bash

test -z "$1" && dr="." || dr="$1"
cd $dr

for i in *.root ; do
  root -l -b -q $HOME/BDTIssues/RatioPlot/myMacro_legendInside.C\(\"$i\"\)
done
