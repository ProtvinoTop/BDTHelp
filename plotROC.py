#!/usr/bin/env python

import sys
import os
import ROOT

f = ROOT.TFile(sys.argv[1])

for i in [f.Get(x.GetName()) for x in f.GetListOfKeys()]:
  c = ROOT.TCanvas()
  i.SetTitle(i.GetName().replace("ROC_", ""))
  t = ROOT.TText(0.1, 0.2, "AUC: {0:0.2f}%".format(i.Integral() - 50  ))
  i.SetMinimum(0)
  i.SetMaximum(1)
  i.Draw()
  t.Draw()
  c.SaveAs(os.path.join(os.path.dirname(sys.argv[1]), i.GetName() + ".png"))
f.Close()
