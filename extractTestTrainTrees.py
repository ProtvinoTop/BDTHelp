#!/usr/bin/env python

import sys
import ROOT

for fi in sys.argv[1:]:
  f = ROOT.TFile(fi, 'readonly')
  testt = f.Get("TestTree")
  traint = f.Get("TrainTree")
  fn = fi.replace(".root", ".test_train_extracted.root")
  newf = ROOT.TFile(fn, 'recreate')
  newtest = testt.CloneTree()
  newtrain = traint.CloneTree()
  newtest.Write()
  newtrain.Write()
  f.Close()
  newf.Close()
  print "Check", fn, "file for the results"
