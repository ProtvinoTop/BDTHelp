#!/usr/bin/env python

# print chi2, probm,  filename for input root file after TMVAReader.py

import sys
import array
import ROOT

f = ROOT.TFile(sys.argv[1])

if "data" not in [x.GetName() for x in f.GetListOfKeys()]:
  f.Close()
  sys.exit(0)

hdata = f.Get("data")
mcstack = f.Get("MCStack")
mcdata = hdata.Clone()
mcdata.Reset()

for xbin in xrange(1, hdata.GetNbinsX() + 1):
  mcval = 0
  for h in mcstack.GetHists():
    mcval += h.GetBinContent(xbin)
  mcdata.SetBinContent(xbin, mcval)

print hdata.Chi2Test(mcdata, "UW chi2"), hdata.Chi2Test(mcdata, "UW"), sys.argv[1]
f.Close()
