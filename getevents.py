#!/usr/bin/env python

import sys
import ROOT

s = 0
for i in sys.argv[1:]:
  f = ROOT.TFile(i)
  b = [x.GetName() for x in f.GetListOfKeys()][0]
  s += f.Get(b).GetEntries()
  f.Close()

print s
