#!/bin/bash

eval `/afs/cern.ch/sw/lcg/releases/lcgenv/latest/lcgenv -p /afs/cern.ch/sw/lcg/releases/LCG_81 x86_64-slc6-gcc49-opt ROOT`
#eval `/afs/cern.ch/sw/lcg/releases/lcgenv/latest/lcgenv -p /afs/cern.ch/sw/lcg/releases/LCG_80 x86_64-slc6-gcc49-dbg Python`
eval `/afs/cern.ch/sw/lcg/releases/lcgenv/latest/lcgenv -G -p /afs/cern.ch/sw/lcg/releases/LCG_80 x86_64-slc6-gcc49-opt pyanalysis`
#eval `/afs/cern.ch/sw/lcg/releases/lcgenv/latest/lcgenv -G -p /afs/cern.ch/sw/lcg/releases/LCG_80 x86_64-slc6-gcc49-dbg cython 0.22`

export ROOTSYS=$(dirname $(dirname $(which root)))
export LD_LIBRARY_PATH=$ROOTSYS/lib:$PYTHONDIR/lib:$LD_LIBRARY_PATH
export PYTHONPATH=$ROOTSYS/lib:$PYTHONPATH
#cd /afs/cern.ch/user/r/razumov/CMS/Matthias/Diff13; source setVars.sh pxl-vars.txt
