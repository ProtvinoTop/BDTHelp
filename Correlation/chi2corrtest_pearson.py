#!/usr/bin/env python2
from __future__ import print_function

import argparse
import copy
import csv
import fnmatch
import itertools
import math
import os
import sys
import xml.etree.ElementTree as ET
import array

import types

import ROOT
# import chisq
import numpy
from progress import progressBar


def pearsonR(D):
    return numpy.corrcoef(D)


def rapidity(px, py, pz, energy):
    v = ROOT.TLorentzVector()
    v.SetPxPyPzE(px, py, pz, energy)
    pt = v.Pt()
    eta = v.Eta()
    m = v.M()

    nom1 = math.sqrt(m ** 2 + (pt ** 2) * (math.cosh(eta) ** 2))
    nom2 = pt * math.sinh(eta)
    den = math.sqrt(m ** 2 + pt ** 2)

    return abs((nom1 + nom2) / den)


def main():
    # branches = []
    svalues = []
    trees = []
    files = []
    sfnames = []

    showProgress = sys.stdout.isatty()
    # showProgress = False

    ROOT.gStyle.SetOptStat(0)
    ROOT.gROOT.SetBatch(True)

    parser = argparse.ArgumentParser(description='Calculate pearson\'s correlation')
    parser.add_argument('variables', metavar='var', nargs='*',
                        help='variables to test')
    parser.add_argument('-r', '--rootdir', dest='rootdir', action='store',
                        help='directory containing root files',
                        default='/afs/cern.ch/user/r/razumov/work/public/output_root.all/')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-s', '--signal', dest='signal', action='store', type=argparse.FileType('r'),
                       help='file wiht list of signal trees')
    group.add_argument('-b', '--background', dest='background', action='store', type=argparse.FileType('r'),
                       help='file wiht list of signal trees')
    parser.add_argument('-w', '--weightxml', dest="weightxml", action='store', default=".", help="Weight xml file")
    parser.add_argument('--TMVAlib', dest="TMVAlib", action='store', default="/afs/cern.ch/user/g/glatyshe/work/public/top/matthias/Diff13/PxlModules/build/internal/TMVA/libTMVA.so", help="Path to TMVA lib")
    parser.add_argument('-o', '--outfile', dest='outfile', action='store',
                        help='Output file name (without extension)', default='corr')
    parser.add_argument('-d', '--dictfile', dest='dictfile', action='store',
                        help='Histogram axes labels lookup file', default=None)
    parser.add_argument('-Q', dest='quick', action='store_true', help='Quick mode - only use 1% of events')

    args = parser.parse_args()

    quick = args.quick and showProgress

    ROOT.gStyle.SetPaintTextFormat("4.2g")

    tree_names = [line.strip().split(" ", 1)[0] for line in (args.signal if args.signal else args.background) if
                  line.strip() and (not line.startswith('#'))]

    print("Loaded {0} tree name{1}".format(len(tree_names), 's' if len(tree_names) > 1 else ''))

    if len(args.variables) < 2:
        print("ERROR: Please specify at least 2 variables")
        exit(0)

    extravars = ["SingleTop_1__Top_1__Px", "SingleTop_1__Top_1__Py", "SingleTop_1__Top_1__Pz", "SingleTop_1__Top_1__E"]

    uservars = [v for v in args.variables if not v.startswith('#')]
    uservars.extend(extravars)
    variables = copy.copy(uservars)

    print("Looking for files in", args.rootdir)

    for root, dirnames, filenames in os.walk(args.rootdir):
        for rootf in fnmatch.filter(filenames, '*2j1t.root'):
            rootfile = os.path.join(root, rootf)
            tf = ROOT.TFile(rootfile)
            files.append(tf)
            k = tf.GetListOfKeys()[0]
            if k.GetClassName() == "TTree":
                if k.GetName() in tree_names:
                    trees.append(k.ReadObj())
                    sfnames.append(rootfile)

    if not trees:
        print("ERROR: No trees loaded!")
        exit(1)

    print("Verifying variables")
    #print(variables)

    for t in trees:
        for v in variables:
            if v != "SingleTop_1__BDT_OUTPUT":
                if t.GetBranch(v) == None:  # testing 'is None' won't work - pyROOT hack
                    print("Removing variable {0}: not present in tree {1}".format(v, t.GetName()))
                    uservars.remove(v)

        variables = copy.copy(uservars)

    if len(variables) == 0:
        print("No variables left to process!")
        exit(0)

    labels = {}

    if args.dictfile is not None:
        with open(args.dictfile, 'r') as csvfile:
            dictreader = csv.reader(csvfile, delimiter=";", quotechar='"')
            for row in dictreader:
                labels[row[0]] = row[1]

    for i in range(len(trees)):
        trees[i].SetBranchStatus('*', 0)

    for i in range(len(variables)):
        svalues.append([])

    ROOT.TMVA.Tools.Instance()
    bdt_variables = {}
    reader = ROOT.TMVA.Reader("!V:!Color")
    
    tree = ET.parse(args.weightxml)
    root = tree.getroot()
    
    methodname = root.get("Method").split("::")[1]
    
    xml_variables = [x.get("Expression") for x in root.iter("Variable")]
    xml_spectators = [x.get("Expression") for x in root.iter("Spectator")]

    for var in xml_variables:
        bdt_variables[var] = array.array('f', [0])
        reader.AddVariable(var, bdt_variables[var])

    for var in xml_spectators:
        bdt_variables[var] = array.array('f', [0])
        reader.AddSpectator(var, bdt_variables[var])

    print ("Booking MVA method {0}...".format(methodname))
    reader.BookMVA(methodname, args.weightxml)

    for i, t in enumerate(trees):
        print("Reading tree", t.GetName())
        entries = t.GetEntriesFast()
        if entries < 1:
            print("Empty tree found, skipping")
            continue

        if quick:
            entries /= 100

        for var in set(itertools.chain(variables, xml_variables, xml_spectators)):
            if var != 'SingleTop_1__BDT_OUTPUT':
                trees[i].SetBranchStatus(var, 1)

        p = progressBar(maxValue=entries, totalWidth=79)

        for k in range(entries):
            if showProgress:
                p(k)

            try:
                nb = t.GetEntry(k)
            except Exception:
                print("Error processing tree {0} (file {1})!".format(t.GetName(), sfnames[i]))
                raise

            if nb <= 0:
                continue
                
            for var in itertools.chain(xml_variables, xml_spectators):
                bdt_variables[var] = getattr(t, var)

            for j, var in enumerate(variables):
                val = None
                
                if var == "SingleTop_1__Top_1__Eta":
                    px = getattr(t, variables[-4])
                    py = getattr(t, variables[-3])
                    pz = getattr(t, variables[-2])
                    pe = getattr(t, variables[-1])
                    val = rapidity(px, py, pz, pe)
                
                if var == "SingleTop_1__BDT_OUTPUT":
                    val = reader.EvaluateMVA(methodname)
                
                if not val:
                    val = getattr(t, var)
                
                svalues[j].append(val)
        
        print('\n')

    print("Creating canvas")

    c = ROOT.TCanvas("c", "", 1024, 768)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gPad.SetRightMargin(0.15)
    c.cd()

    print("Calculating Pearson's r")
    lvariables = [x.split("__", 1)[1] for x in variables]

    nv = len(variables) - len(extravars)
    print (len(svalues), len(variables), nv)
       
    pr = pearsonR(svalues)

    f = open(args.outfile + ".csv", "w")
    header = [""]
    header.extend(lvariables)
    f.write(";".join(header) + "\n")
    print("=== Chi2 ===")
    print("Var_i", "Var_j", "r_ij")
    for i in range(nv):
        f.write(lvariables[i])
        for j in range(nv):
            print("{0} {1} {2:.2f}".format(lvariables[i], lvariables[j], pr[i][j]))
            f.write(";{0}".format(pr[i][j]))
        f.write("\n")
    f.close()

    h2s = ROOT.TH2F("h2s", "Pearson correlation test ({0})".format("signal" if args.signal else "background"), nv, 0,
                    nv, nv, 0, nv)

    for i in range(nv):
        label = labels[variables[i]] if variables[i] in labels else variables[i]
        h2s.GetXaxis().SetBinLabel(i + 1, label)
        h2s.GetYaxis().SetBinLabel(i + 1, label)
        for j in range(nv):
            h2s.SetBinContent(i + 1, j + 1, pr[i][j])

    h2s.GetXaxis().SetLabelSize(0.05)
    h2s.GetYaxis().SetLabelSize(0.05)
    h2s.GetZaxis().SetLabelSize(0.05)
    h2s.SetMarkerSize(1.25)

    h2s.Draw("COLZ")
    h2s.Draw("TEXT SAME")
    ROOT.gPad.Update()

    for k in h2s.GetListOfFunctions():
        print(k)

    pt = ROOT.TPaveText(0.9, 0.9, 0.95, 0.95, "ndc")
    pt.SetBorderSize(0)
    pt.SetFillColor(0)
    pt.AddText("r_{ij}")
    pt.SetTextFont(42)
    pt.SetTextSize(0.02972973)
    pt.Draw()

    c.SaveAs("{0}/{0}_R.root".format(args.outfile))
    c.SaveAs("{0}/{0}_R.C".format(args.outfile))
    c.SaveAs("{0}/{0}_R.png".format(args.outfile))
    c.SaveAs("{0}/{0}_R.pdf".format(args.outfile))


if __name__ == "__main__":
    main()
