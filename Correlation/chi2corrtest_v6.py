#!/usr/bin/env python2
from __future__ import print_function

import argparse
import copy
import csv
import fnmatch
import itertools
import math
import os
import sys
import array
import xml.etree.ElementTree as ET

import ROOT
# import chisq
import scipy.stats  # import chi2
from progress import progressBar


def chisqprob(x, df):
    return scipy.stats.chi2(df).sf(x)


def chi2matthias(x, y, xlabel, ylabel):
    N = len(x)

    x_sorted = sorted(zip(range(N), x), key=lambda q: q[1])
    y_sorted = sorted(zip(range(N), y), key=lambda q: q[1])

    x_tilde = list(itertools.repeat(0, N))
    y_tilde = list(itertools.repeat(0, N))

    for i in range(N):
        q = x_sorted[i]
        x_tilde[q[0]] = 1.0 * i / N

        q = y_sorted[i]
        y_tilde[q[0]] = 1.0 * i / N

    n = int(round(math.sqrt(N) / 5))
    e = N / (n ** 2)
    chi2 = 0

    h2 = ROOT.TH2D("h2", "{0} vs {1};{0};{1}".format(xlabel, ylabel), n, 0, 1, n, 0, 1)

    for i in range(N):
        h2.Fill(x_tilde[i], y_tilde[i])
        h2.Draw("COLZ")

    for i in range(n):
        for j in range(n):
            chi2 += ((h2.GetBinContent(i + 1, j + 1) - e) ** 2 / e)

            #    c.SaveAs("{2}/chi2{2}-{0}-{1}.png".format(xlabel, ylabel, prefix))
            #    c.SaveAs("{2}/chi2{2}-{0}-{1}.pdf".format(xlabel, ylabel, prefix))

    h2 = None
    ndof = n ** 2 - (2 * n - 1)
    prob = chisqprob(chi2, ndof)
    return chi2, ndof, prob


def rapidity(px, py, pz, energy):
    v = ROOT.TLorentzVector()
    v.SetPxPyPzE(px, py, pz, energy)
    pt = v.Pt()
    eta = v.Eta()
    m = v.M()

    nom1 = math.sqrt(m ** 2 + (pt ** 2) * (math.cosh(eta) ** 2))
    nom2 = pt * math.sinh(eta)
    den = math.sqrt(m ** 2 + pt ** 2)

    return abs((nom1 + nom2) / den)


def main():
    # branches = []
    svalues = []
    trees = []
    files = []
    fnames = []

    if sys.stdout.isatty():
        showProgress = True
    else:
        showProgress = False

    # showProgress = False

    ROOT.gStyle.SetOptStat(0)
    ROOT.gROOT.SetBatch(True)

    parser = argparse.ArgumentParser(description='Calculate chi**2 correlation')
    parser.add_argument('variables', metavar='var', nargs='*',
                        help='variables to test')
    parser.add_argument('-r', '--rootdir', dest='rootdir', action='store',
                        help='directory containing root files',
                        default='/afs/cern.ch/user/r/razumov/work/public/output_root.all/')
    parser.add_argument('-w', '--weightxml', dest="weightxml", action='store', default=".", help="Weight xml file")
    parser.add_argument('--TMVAlib', dest="TMVAlib", action='store', default="/afs/cern.ch/user/g/glatyshe/work/public/top/matthias/Diff13/PxlModules/build/internal/TMVA/libTMVA.so", help="Path to TMVA lib")
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-s', '--signal', dest='signal', action='store', type=argparse.FileType('r'),
                       help='file wiht list of signal trees')
    group.add_argument('-b', '--background', dest='background', action='store', type=argparse.FileType('r'),
                       help='file wiht list of background trees')    
    parser.add_argument('-o', '--outfile', dest='outfile', action='store',
                        help='Output file name (without extension)', default='corr')
    parser.add_argument('-d', '--dictfile', dest='dictfile', action='store',
                        help='Histogram axes labels lookup file', default=None)
    parser.add_argument('-p', '--probability', dest='prob', action='store_true',
                        help='Calculate probability instead of chi**2/ndf')
    parser.add_argument('-Q', '--Quick', dest='quick', action='store_true',
                        help='Only use 1% of the events')

    args = parser.parse_args()

    if args.prob:
        ROOT.gStyle.SetPaintTextFormat("4.2g")

    tree_names = [line.strip().split(" ", 1)[0] for line in (args.signal if args.signal else args.background) if
                  line.strip() and (not line.startswith('#'))]

    print("Loaded {0} tree name{1}".format(len(tree_names), 's' if len(tree_names) > 1 else ''))

    if len(args.variables) < 2:
        print("ERROR: Please specify at least 2 variables")
        exit(0)

    extravars = ["SingleTop_1__Top_1__Px", "SingleTop_1__Top_1__Py", "SingleTop_1__Top_1__Pz", "SingleTop_1__Top_1__E"]

    uservars = [v for v in args.variables if not v.startswith('#')]
    uservars.extend(extravars)
    variables = copy.copy(uservars)

    print("Looking for files in", args.rootdir)

    for root, dirnames, filenames in os.walk(args.rootdir):
        for rootf in fnmatch.filter(filenames, '*2j1t.root'):
            rootfile = os.path.join(root, rootf)
            tf = ROOT.TFile(rootfile)
            files.append(tf)
            k = tf.GetListOfKeys()[0]
            if k.GetClassName() == "TTree":
                if k.GetName() in tree_names:
                    trees.append(k.ReadObj())
                    fnames.append(rootfile)

    if not trees:
        print("ERROR: No trees loaded!")
        exit(1)

    print("Verifying variables")

    for t in trees:
        for v in variables:
            if v != "SingleTop_1__BDT_OUTPUT":
                if t.GetBranch(v) == None:  # testing 'is None' won't work - pyROOT hack
                    print("Removing variable {0}: not present in tree {1}".format(v, t.GetName()))
                    uservars.remove(v)

        variables = copy.copy(uservars)

    if len(variables) == 0:
        print("No variables left to process!")
        exit(0)

    labels = {}

    if args.dictfile is not None:
        with open(args.dictfile, 'r') as csvfile:
            dictreader = csv.reader(csvfile, delimiter=";", quotechar='"')
            for row in dictreader:
                labels[row[0]] = row[1]

    for i in range(len(trees)):
        trees[i].SetBranchStatus('*', 0)

    for i in range(len(variables)):
        svalues.append([])

    ROOT.TMVA.Tools.Instance()
    bdt_variables = {}
    reader = ROOT.TMVA.Reader("!V:!Color")
    
    tree = ET.parse(args.weightxml)
    root = tree.getroot()
    
    methodname = root.get("Method").split("::")[1]
    
    xml_variables = [x.get("Expression") for x in root.iter("Variable")]
    xml_spectators = [x.get("Expression") for x in root.iter("Spectator")]
        
    for var in xml_variables:
        bdt_variables[var] = array.array('f', [0])
        reader.AddVariable(var, bdt_variables[var])

    for var in xml_spectators:
        bdt_variables[var] = array.array('f', [0])
        reader.AddSpectator(var, bdt_variables[var])

    print ("Booking MVA method {0}...".format(methodname))
    reader.BookMVA(methodname, args.weightxml)

    for i, t in enumerate(trees):
        print("Reading tree", t.GetName())
        entries = t.GetEntriesFast()
        if entries < 1:
            print("Empty tree found, skipping")
            continue
       
        for var in set(itertools.chain(variables, xml_variables, xml_spectators)):
            if var != 'SingleTop_1__BDT_OUTPUT':
                trees[i].SetBranchStatus(var, 1)
            
        if args.quick:
            entries /= 100

        p = progressBar(maxValue=entries, totalWidth=79)
        for k in range(entries):
            if showProgress:
                p(k)

            try:
                nb = t.GetEntry(k)
            except Exception:
                print("Error processing signal tree {0} (file {1})!".format(t.GetName(), fnames[i]))
                raise

            if nb <= 0:
                continue

            for var in itertools.chain(xml_variables, xml_spectators):
                bdt_variables[var] = getattr(t, var)

            for j, var in enumerate(variables):
                if var == "SingleTop_1__Top_1__Eta":
                    px = getattr(t, variables[-4])
                    py = getattr(t, variables[-3])
                    pz = getattr(t, variables[-2])
                    pe = getattr(t, variables[-1])
                    svalues[j].append(rapidity(px, py, pz, pe))
                    continue
                
                if var == "SingleTop_1__BDT_OUTPUT":
                    svalues[j].append(reader.EvaluateMVA(methodname))
                    continue
                
                svalues[j].append(getattr(t, var))

        print('\n')

    print("Creating canvas")

    c = ROOT.TCanvas("c", "", 1024, 768)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gPad.SetRightMargin(0.15)
    c.cd()

    print("Calculating chi**2")
    lvariables = [x.split("__", 1)[1] for x in variables]

    nv = len(variables)
    chi2s = list(itertools.repeat([], nv))
    Ndofs = list(itertools.repeat([], nv))
    probs = list(itertools.repeat([], nv))
    for i in range(nv):
        chi2s[i] = list(itertools.repeat(0, nv))
        Ndofs[i] = list(itertools.repeat(1, nv))
        probs[i] = list(itertools.repeat(0, nv))

    nv -= len(extravars)

    for i in range(nv):
        print("Variable #{1}/{2}: {0}".format(lvariables[i], i + 1, nv))
        for j in range(i + 1, nv):
            res = chi2matthias(svalues[i], svalues[j], lvariables[i], lvariables[j])
            chi2s[i][j] = res[0]
            chi2s[j][i] = res[0]
            Ndofs[i][j] = res[1]
            Ndofs[j][i] = res[1]
            probs[i][j] = res[2]
            probs[j][i] = res[2]

    f = open(args.outfile + ".csv", "w")
    header = [""]
    header.extend(lvariables)
    f.write(";".join(header) + "\n")
    print("=== Chi2 ===")
    print("Var1", "Var2", "chi**2/Ndof", "Ndof", "Prob")
    for i in range(nv):
        f.write(lvariables[i] + ";")
        for j in range(nv):
            print("{0} {1} {2:.2f} {3} {4}".format(lvariables[i], lvariables[j], chi2s[i][j], Ndofs[i][j],
                                                   probs[i][j]))
            f.write("{0};{1};{2}".format(chi2s[i][j] / Ndofs[i][j], Ndofs[i][j], probs[i][j]))
        f.write("\n")
    f.close()

    h2s = ROOT.TH2F("h2s", "#chi^{{2}}-based correlations ({0})".format("signal" if args.signal else "background"), nv,
                    0, nv, nv, 0, nv)

    for i in range(nv):
        label = labels[variables[i]] if variables[i] in labels else variables[i]
        h2s.GetXaxis().SetBinLabel(i + 1, label)
        h2s.GetYaxis().SetBinLabel(i + 1, label)
        for j in range(nv):
            if args.prob:
                h2s.SetBinContent(i + 1, j + 1, probs[i][j])
            else:
                h2s.SetBinContent(i + 1, j + 1, chi2s[i][j] / Ndofs[i][j])

    h2s.GetXaxis().SetLabelSize(0.05)
    h2s.GetYaxis().SetLabelSize(0.05)
    h2s.GetZaxis().SetLabelSize(0.05)
    h2s.SetMarkerSize(1.25)

    h2s.Draw("COLZ")
    h2s.Draw("TEXT SAME")
    ROOT.gPad.Update()

    pt = ROOT.TPaveText(0.85, 0.9, 0.9, 0.95, "ndc")
    pt.SetBorderSize(0)
    pt.SetFillColor(0)
    if args.prob:
        pt.AddText("P(#chi^{2})")
    else:
        pt.AddText("#chi^{2}/Ndof")
    pt.SetTextFont(42)
    pt.SetTextSize(0.02972973)
    pt.Draw()

    c.SaveAs("{0}/{0}_Chi2{1}.root".format(args.outfile, 'P' if args.prob else ''))
    c.SaveAs("{0}/{0}_Chi2{1}.C".format(args.outfile, 'P' if args.prob else ''))
    c.SaveAs("{0}/{0}_Chi2{1}.png".format(args.outfile, 'P' if args.prob else ''))
    c.SaveAs("{0}/{0}_Chi2{1}.pdf".format(args.outfile, 'P' if args.prob else ''))


if __name__ == "__main__":
    main()
